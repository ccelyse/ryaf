<?php

namespace App\Http\Controllers\api\v1;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use \Validator;

class LoginController extends Controller
{
    public function login(Request $request){
        $validator = Validator::make($request->all(),[
            'email' => 'required',
            'password' => 'required'
        ]);
        if ($validator->fails()){
            return response()->json([
                'response_status'=> 400,
                'message'=> "Bad request",
            ]);
        }
        $credentials = request(['email','password']);

        $user = User::where('email',$request['email'])->first();

//        if (! $user || ! Hash::check($request->password, $user->password)) {
//            throw ValidationException::withMessages([
//                'email' => ['The provided credentials are incorrect.'],
//            ]);
//        }

        if( !Auth::attempt($credentials)){
            return response()->json([
                'response_status'=> 400,
                'message'=> "Invalid login credentials",
            ]);
        }

        $accessToken = Auth::user()->createToken('authToken')->accessToken;

        return response([
            'response_status'=> 200,
            'user' => Auth::user(),
            'access_token' => $accessToken]);

    }
    public function register(Request $request){
        $validator = Validator::make($request->all(),[
            'first_name' => 'required',
            'last_name' => 'required',
            'age' => 'required',
            'gender' => 'required',
            'country_id' => 'required',
            'phone_number' => 'required',
            'password' => 'required',
            'email' => [
                'required',
                'string',
                'email',
                'max:255',
                Rule::unique(User::class),
            ],
        ]);
        if ($validator->fails()){
            return response()->json([
                'response_status'=> 400,
                'message'=> "Bad request",
            ]);
        }
        $names = $request['first_name'].' '.$request['last_name'];
        $newUser = new User();
        $newUser->name = $names;
        $newUser->age = $request->age;
        $newUser->gender = $request->gender;
        $newUser->country_id = $request->country_id;
        $newUser->phone_number = $request->phone_number;
        $newUser->email = $request->email;
        $newUser->phone_number = $request->phone_number;
        $newUser->status = "Active";
        $newUser->role = "Reader";
        $newUser->password = Hash::make($request->password);

        if ($request->hasFile('image_name')) {
            $destinationPath = public_path('/storage/UserImages');
            $files = $request->file('image_name'); // will get all files
//            dd($files);
            $file_name = rand() . '.' . $files->getClientOriginalExtension(); //Get file original name
            $files->move($destinationPath , $file_name); // move files to destination folder
            $newUser->image_name = $file_name;
        }

        return response()->json([
            'response_status'=> 200,
            'message'=> "User successfully registered",
        ]);
    }
}
