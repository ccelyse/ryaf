<?php

namespace App\Http\Controllers\api\v1;

use App\Http\Controllers\Controller;
use App\Models\AuthorDetails;
use App\Models\BillingAddressPrimary;
use App\Models\BookGenres;
use App\Models\Books;
use App\Models\BookSold;
use App\Models\BookTransactions;
use App\Models\Country;
use App\Models\ReaderBillingAddress;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use \Validator;

class MobileController extends Controller
{
    public function MyAccountDashboard(Request $request){
        $user_id = $request['user_id'];
//        $user_id = "2";
        $orders = BookTransactions::where('user_id',$user_id)->count();
        $books = BookSold::where('user_id',$user_id)->count();
        $address = ReaderBillingAddress::where('user_id',$user_id)->count();
        return response()->json([
            'response_status' =>200,
            'order' => $orders,
            'books' => $books,
            'address' => $address,
        ]);
    }
    public function MyAccount(Request $request){
        $user_id = $request['user_id'];
        $UserDetails = User::where('users.id',$user_id)
            ->join('apps_countries', 'apps_countries.id', '=', 'users.country_id')
            ->select('users.*', 'apps_countries.country_name')
            ->get();
        return response()->json([
            'response_status' =>200,
            'user_info' => $UserDetails,
        ]);
    }

    public function UpdateMyAccount(Request $request){
        $validator = Validator::make($request->all(),[
            'name' => 'required',
            'phone_number' => 'required',
            'email' => 'required',
            'gender' => 'required',
            'country_id' => 'required',
        ]);
        if ($validator->fails()){
            return response()->json([
                'response_status'=> 200,
                'message'=> "One of the field is empty",
            ]);
        }
        User::updateOrCreate(['id' => $request->user_id], [
            'name' => $request->name,
            'phone_number' => $request->phone_number,
            'email' => $request->email,
            'gender' => $request->gender,
            'age' => $request->age,
            'country_id' => $request->country_id,
        ]);

        return response()->json([
            'response_status' =>200,
            'response_message' => "User account  successfully updated",
        ]);
    }

    public function ChangePassword(Request $request){
        $userpass = User::where('id',$request->user_id)->value('password');
        if (! Hash::check($request->currentPass, $userpass)) {
            return response()->json([
                'response_status' =>400,
                'response_message' => "The provided password does not match your current password.",
            ]);
        }else{
            $user = User::find($request->user_id);
            $user->password = Hash::make($request->newPass);
            $user->save();
            return response()->json([
                'response_status' =>200,
                'response_message' => "User password  successfully updated.",
            ]);
        }
    }
    public function MyOrders(Request $request){
        $booksold = BookTransactions::where('user_id',$request->user_id)
            ->join('apps_countries', 'apps_countries.id', '=', 'book_transactions.country_id')
            ->select('book_transactions.*', 'apps_countries.country_name')
            ->get();
        foreach ($booksold as $book){
            $this->BookOrdered($book);
            $book['BookOrdered'] = $this->BookOrdered($book);
        }
        if(0 == count($booksold)){
            return response()->json([
                'response_message' => "failed",
                'response_status' =>400
            ]);
        }else{
            return $booksold->toArray();
        }
    }
    public function BookOrdered($book){

        $bookordered = BookSold::where('transaction_id',$book->transaction_id)->get();
        foreach ($bookordered as $data){
            $ids = $data->book_id;
        }
        $ids= array();
        foreach($bookordered as $data){
            array_push($ids, $data->book_id);
        }
        $book_details = Books::whereIn('books.id',$ids)->join('users', 'users.id', '=', 'books.user_id')
            ->join('book_genres', 'book_genres.id', '=', 'books.book_category')
            ->select('books.*', 'users.name','book_genres.genre_name')
            ->get();

        foreach ($book_details as $details){
            $book_names = $details->book_name;
        }
        $book_names= array();
        foreach($book_details as $details){
            array_push($book_names, $details->book_name);
        }
//        dd($details);
        return $book_details->toArray();
//        return json_encode($book_names);
    }
    public function Country(){
        $country = Country::all();
        if(0 == count($country)){
            return response()->json([
                'response_message' => "failed",
                'response_status' =>400
            ]);
        }else{
            return $country->toArray();
        }
    }
    public function Authors(){
        $authors_id = AuthorDetails::all();
        foreach ($authors_id as $data){
            $ids = $data->user_id;
        }
        $ids= array();
        foreach($authors_id as $data){
            array_push($ids, $data->user_id);
        }
        $authors = User::whereIn('id',$ids)->where('role',"Author")->get();
        if(0 == count($authors)){
            return response()->json([
                'response_message' => "failed",
                'response_status' =>400
            ]);
        }else{
            return $authors->toArray();
        }
    }
    public function Genre(){
        $Genre = BookGenres::limit('6')->get();
        if(0 == count($Genre)){
            return response()->json([
                'response_message' => "failed",
                'response_status' =>400
            ]);
        }else{
            return $Genre->toArray();
        }
    }
    public function MyBooks(Request $request){

        $booksold = BookTransactions::where('user_id',$request->user_id)
            ->join('apps_countries', 'apps_countries.id', '=', 'book_transactions.country_id')
            ->select('book_transactions.*', 'apps_countries.country_name')
            ->get();

        foreach ($booksold as $sold){
            $ids_sold = $sold->transaction_id;
        }
        $ids_sold= array();
        foreach($booksold as $sold){
            array_push($ids_sold, $sold->transaction_id);
        }
        $bookordered = BookSold::whereIn('transaction_id',$ids_sold)->get();

        foreach ($bookordered as $data){
            $ids = $data->book_id;
        }
        $ids= array();
        foreach($bookordered as $data){
            array_push($ids, $data->book_id);
        }
        $book_details = Books::whereIn('books.id',$ids)->join('users', 'users.id', '=', 'books.user_id')
            ->join('book_genres', 'book_genres.id', '=', 'books.book_category')
            ->select('books.*', 'users.name','book_genres.genre_name')
            ->get();

        foreach ($book_details as $book){
            $this->BookOrderInfo($book);
            $book['BookOrderInfo'] = $this->BookOrderInfo($book);
        }
//        dd($book_details);
        return $book_details->toArray();

//
//        if(0 == count($booksold)){
//            return response()->json([
//                'response_message' => "failed",
//                'response_status' =>400
//            ]);
//        }else{
//            return $booksold->toArray();
//        }

    }
    public function BookOrderInfo($book){
        $bookordered = BookSold::where('book_id',$book->id)->value('transaction_id');
        $booksold = BookTransactions::where('transaction_id',$bookordered)->get();
//        dd($bookordered);
        return $booksold->toArray();
    }
    public function UserBookOrdered($book){
        $bookordered = BookSold::where('transaction_id',$book->transaction_id)->get();
        foreach ($bookordered as $data){
            $ids = $data->book_id;
        }
        $ids= array();
        foreach($bookordered as $data){
            array_push($ids, $data->book_id);
        }
        $book_details = Books::whereIn('books.id',$ids)->join('users', 'users.id', '=', 'books.user_id')
            ->join('book_genres', 'book_genres.id', '=', 'books.book_category')
            ->select('books.*', 'users.name','book_genres.genre_name')
            ->get();
        return $book_details->toArray();
    }

    public function MyAddress(Request $request)
    {
        $Address = ReaderBillingAddress::where('user_id',$request->user_id)
            ->join('apps_countries', 'apps_countries.id', '=', 'reader_billing_addresses.country_id')
            ->select('reader_billing_addresses.*', 'apps_countries.country_name')
            ->get();

        foreach ($Address as $data){
            $this->PrimaryAddress($data);
            $data['PrimaryAddress'] = $this->PrimaryAddress($data);
        }

        return response()->json([
            'response_message' => "success",
            'response_status' =>200,
            'Address' => $Address
        ]);
    }

    public function PrimaryAddress($data){
        $address = BillingAddressPrimary::where('reader_billing_addresses_id',$data->id)->value('address_primary');
        return json_encode($address);
    }
    public function MakePrimary(Request $request)
    {
        $check_primary = BillingAddressPrimary::where('user_id',$request->user_id)->get();
        if(count($check_primary) > 0){
            $update = BillingAddressPrimary::where('user_id',$request->user_id)->update(['address_primary'=>'Yes','reader_billing_addresses_id'=>$request->id]);
            return response()->json([
                'response_message' => "successfully updated address book.",
                'response_status' =>200,
            ]);
        }else{
            BillingAddressPrimary::create([
                'user_id' => $request->user_id,
                'reader_billing_addresses_id' => $request->id,
                'address_primary' => "Yes",
            ]);
            return response()->json([
                'response_message' => "successfully updated address book.",
                'response_status' =>200,
            ]);
        }
    }
    public function AddAddress(Request $request){
        ReaderBillingAddress::updateOrCreate(['id' => $request->id],[
            'user_id' => $request->user_id,
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'company_name' => $request->company_name,
            'country_id' => $request->country_id,
            'street_address_house_number' => $request->street_address_house_number,
            'town_city' => $request->town_city,
            'post_code' => $request->post_code,
            'phone_number' => $request->phone_number,
            'email' => $request->email,
        ]);
        return response()->json([
            'response_status' =>200,
            'response_message' => "Billing Address successfully added."
        ]);
    }
    public function removeAddress(Request $request)
    {
        $delete_primary = BillingAddressPrimary::where('reader_billing_addresses_id',$request->id)->delete();
        $delete = ReaderBillingAddress::find($request->id);
        $delete->delete();
        return response()->json([
            'response_status' =>200,
            'response_message' => "Address successfully deleted."
        ]);
    }
    public function FeaturedCategories(){
        $categories  = BookGenres::all();
        if(0 == count($categories)){
            return response()->json([
                'response_message' => "failed",
                'response_status' =>400
            ]);
        }else{
            return $categories->toArray();
        }
    }
    public function GenreBooks(Request $request){
        $books = Books::where('book_category',$request['id'])->join('users', 'users.id', '=', 'books.user_id')
            ->join('book_genres', 'book_genres.id', '=', 'books.book_category')
            ->select('books.*', 'users.name','book_genres.genre_name')
            ->get();
        if(0 == count($books)){
            return response()->json([
                'response_message' => "failed",
                'response_status' =>400
            ]);
        }else{
            return $books->toArray();
        }
    }
    public function NewRelease(){
        $book = Books::join('users', 'users.id', '=', 'books.user_id')
            ->select('books.*', 'users.name')
            ->limit(6)
            ->orderBy('id','desc')
            ->get();
        $book->makeHidden(['book_file_name']);
        if(0 == count($book)){
            return response()->json([
                'response_message' => "failed",
                'response_status' =>400
            ]);
        }else{
            return $book->toArray();
        }
    }
    public function AllBooks(){
        $book = Books::join('users', 'users.id', '=', 'books.user_id')
            ->select('books.*', 'users.name')
            ->orderBy('id','desc')
            ->get();
        $book->makeHidden(['book_file_name']);
        if(0 == count($book)){
            return response()->json([
                'response_message' => "failed",
                'response_status' =>400
            ]);
        }else{
            return $book->toArray();
        }
    }
    public function PopularBooks(){
        $book = Books::join('users', 'users.id', '=', 'books.user_id')
            ->select('books.*', 'users.name')
            ->limit(8)
            ->orderBy('id','desc')
            ->get();
        $book->makeHidden(['book_file_name']);
        if(0 == count($book)){
            return response()->json([
                'response_message' => "failed",
                'response_status' =>400
            ]);
        }else{
            return $book->toArray();
        }
    }
    public function BookMore(Request $request){
        $books = Books::where('books.id',$request['id'])->join('users', 'users.id', '=', 'books.user_id')
            ->join('book_genres', 'book_genres.id', '=', 'books.book_category')
            ->select('books.*', 'users.name','book_genres.genre_name')
            ->get();
        $books->makeHidden(['book_file_name']);
        if(0 == count($books)){
            return response()->json([
                'response_message' => "failed",
                'response_status' =>400
            ]);
        }else{
            return $books->toArray();
        }
    }
    public function AuthorProfile(Request $request){
        $author = User::where('users.id',$request['id'])->join('author_details', 'author_details.user_id', '=', 'users.id')
            ->join('apps_countries', 'apps_countries.id', '=', 'users.country_id')
            ->select('author_details.*', 'users.name','users.email','users.country_id','users.image_name','apps_countries.country_name')
            ->get();
        $books = Books::where('user_id',$request['id'])
            ->join('users', 'users.id', '=', 'books.user_id')
            ->select('books.*', 'users.name')
            ->limit(4)
            ->orderBy('id','desc')
            ->get();
        $books->makeHidden(['book_file_name']);

        $author_books = Books::where('books.user_id',$request['id'])
            ->join('users', 'users.id', '=', 'books.user_id')
            ->join('book_genres', 'book_genres.id', '=', 'books.book_category')
            ->select('books.*', 'users.name','book_genres.genre_name')
            ->orderBy('id','desc')
            ->get();
        $author_books->makeHidden(['book_file_name']);

        if(0 == count($author)){
            return response()->json([
                'response_message' => "Author can not be found",
                'response_status' =>400
            ]);
        }else{
            return response()->json([
                'Author_info' => $author,
                'Author_books' =>$author_books
            ]);
        }
    }
}
