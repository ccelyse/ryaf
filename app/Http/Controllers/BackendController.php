<?php

namespace App\Http\Controllers;

use App\Facades\Cart as CartFacade;
use App\Models\Country;
use App\Models\User;
use App\Models\user_business_information;
//use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use PDF;

class BackendController extends Controller
{
    public function index()
    {

        $role = Auth::user()->role;
//        dd($role);
        $checkrole = explode(',', $role);
//        dd($checkrole);

        if (in_array('Staff', $checkrole) and Auth::user()->status == "Approved") {
//            dd($role);
            return redirect('/Staff/Dashboard');
        }elseif(in_array('Admin', $checkrole) and Auth::user()->status == "Approved"){
            return redirect('/Admin/Dashboard');
        }else{
            Auth::logout();
            Session::flush();
            return redirect('/Login');
        }

    }
    public function login(){
        return view('auth.login');
    }
    public function DownloadCertificate(Request $request){
        $data = user_business_information::where('user_id', $request['id'])->first();
        $user_date = User::where('id',$request['id'])->value('created_at');
        $business_name  = $data->businessName;
        $new_date = date_format($user_date, "Y");
        $new_date_v1 = date_format($user_date, "Y-m-d");
        view()->share('downloadCertificate',$data);
//        $pdf = PDF::loadView('downloadCertificate', $data);
        $pdf = PDF::loadView('downloadCertificate', compact('business_name','new_date','new_date_v1'))->setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true]);
        return $pdf->download('MemberCertificate.pdf');
//        return view('downloadCertificate')->with(['user_date'=>$new_date_v1,'new_date'=>$new_date,'business_name'=>$business_name]);
    }
}
