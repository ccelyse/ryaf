<?php

namespace App\Http\Livewire\Backend\Admin;

use App\Models\businesType;
use Livewire\Component;

class Businesstype extends Component
{
    public $BusinessType;
    public $business_types_name;
    public $name;
    public $type_id;
    public $updateMode = false;
    public function submit()
    {
//        dd($this);
        $validatedData = $this->validate([
            'business_types_name' => ['required'],
        ]);
        $newData = businesType::create([
            'business_types_name' => $this->business_types_name,
        ]);
        $this->clearForm();
        $this->emit('userRecord'); // Close model to using to jquery
        session()->flash('success', 'You have successfully created new record');
    }
    public function edit($id){
        $this->type_id = $id;
        $info = businesType::where('id',$id)->first();
        $this->name = $info->business_types_name;
    }
    public function update(){
//        dd($this);
        if ($this->type_id) {
            $update_type = \App\Models\businesType::find($this->type_id);
            $update_type->business_types_name =  $this->name;
            $update_type->save();
            $this->updateMode = false;
            session()->flash('success', 'Successfully Updated record.');
            $this->clearForm();
            $this->emit('userUpdate'); // Close model to using to jquery

        }
    }
    public function delete($id){
        $delete = businesType::where('id',$id)->delete();
        session()->flash('success', 'You have successfully created new record');
    }
    public function clearForm(){
        $this->business_types_name = '';
        $this->name = '';
    }
    public function render()
    {
        $this->BusinessType = businesType::all();
        return view('livewire.backend.admin.businesstype')->layout('Layouts.BackendMaster');
    }
}
