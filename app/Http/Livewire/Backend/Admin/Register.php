<?php

namespace App\Http\Livewire\Backend\Admin;

use Livewire\Component;

class Register extends Component
{
    public function render()
    {
        return view('livewire.backend.admin.register');
    }
}
