<?php

namespace App\Http\Livewire\Backend\Admin;

use App\Models\businesSubType;
use App\Models\businesType;
use Livewire\Component;

class Subbusinesstype extends Component
{
    public $BusinessType;
    public $subBusinessType;
    public $business_types_name;
    public $business_type_id;
    public $sub_name;
    public $name;
    public $type_id;
    public $updateMode = false;
    public function submit()
    {
//        dd($this);
        $validatedData = $this->validate([
            'sub_name' => ['required'],
            'business_type_id' => ['required'],
        ]);
        $newData = businesSubType::create([
            'sub_name' => $this->sub_name,
            'business_type_id' => $this->business_type_id,
        ]);
        $this->clearForm();
        $this->emit('userRecord'); // Close model to using to jquery
        session()->flash('success', 'You have successfully created new record');
    }
    public function edit($id){
        $this->type_id = $id;
        $info = businesSubType::where('id',$id)->first();
        $this->name = $info->business_types_name;
    }
    public function update(){
//        dd($this);
        if ($this->type_id) {
            $update_type = \App\Models\businesType::find($this->type_id);
            $update_type->business_types_name =  $this->name;
            $update_type->save();
            $this->updateMode = false;
            session()->flash('success', 'Successfully Updated record.');
            $this->clearForm();
            $this->emit('userUpdate'); // Close model to using to jquery

        }
    }
    public function delete($id){
        $delete = businesType::where('id',$id)->delete();
        session()->flash('success', 'You have successfully created new record');
    }
    public function clearForm(){
        $this->business_types_name = '';
        $this->name = '';
        $this->sub_name = '';
    }
    public function render()
    {
        $this->BusinessType = businesType::all();
        $this->subBusinessType = businesSubType::
        join('business_types', 'business_types.id', '=', 'busines_sub_types.business_type_id')
        ->select('busines_sub_types.*','business_types.business_types_name')
        ->get();
        return view('livewire.backend.admin.subbusinesstype')->layout('Layouts.BackendMaster');
    }
}
