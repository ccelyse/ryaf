<?php

namespace App\Http\Livewire\Backend\Admin;

use App\Models\cell;
use App\Models\district;
use App\Models\provinces;
use App\Models\sector;
use App\Models\User;
use App\Models\user_business_information;
use App\Models\user_personal_information;
use App\Models\village;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;
use Livewire\Component;

class Staff extends Component
{
    public $all_members;
    public $provinces =[];
    public $maritalStatus;
    public $title;
    public $nationalDNo;
    public $province;
    public $district;
    public $sector;
    public $cell;
    public $village;
    public $names;
    public $email;
    public $phonenumber;
    public $password;
    public $gender;
    public $districts=[];
    public $sectors=[];
    public $cells=[];
    public $villages=[];

    public function submit(){
//        dd($this);
        $validatedData = $this->validate([
            'names' => ['required'],
            'phonenumber' => ['required'],
            'gender' => ['required'],
//            'password' => ['required'],
            'title' => ['required'],
            'nationalDNo' => ['required'],
            'province' => ['required'],
            'district' => ['required'],
            'sector' => ['required'],
            'cell' => ['required'],
            'village' => ['required'],
            'email' => ['required', 'max:255','unique:users']
        ]);
        $currentMonth = Carbon::now()->format('m');
        $pass = mt_rand(10, 9999). $currentMonth;
        $number_format = "25".$this->phonenumber;
        $newUser = User::create([
            'name' => $this->names,
            'email' => $this->email,
            'phone_number' => $this->phonenumber,
            'status' => "Approved",
            'role' => "Staff",
            'gender' => $this->gender,
            'title' => $this->title,
            'nationalDNo' => $this->nationalDNo,
            'province' => $this->province,
            'district' => $this->district,
            'sector' => $this->sector,
            'cell' => $this->cell,
            'village' => $this->village,
            'password' => Hash::make($pass),
        ]);
        $message = "Your account have been successfully created and here is your password: " .$pass;
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://api.mista.io/sms',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => array('to' => $number_format,'from' => 'RYAF','unicode' => '0','sms' => $message,'action' => 'send-sms'),
            CURLOPT_HTTPHEADER => array(
                'x-api-key:1698cb44-330e-9b64-a160-c1d2dadf13c8-6e05ec6d'
            ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $this->clearForm();
        $this->emit('userUpdate'); // Close model to using to jquery
        session()->flash('success', 'You have successfully created staff account');
//        return redirect(url('/SignIn'));
    }
    public function clearForm()
    {
        $this->names = '';
        $this->email = '';
        $this->phonenumber = '';
        $this->gender = '';
        $this->password = '';
        $this->title = '';
        $this->nationalDNo = '';
        $this->province = [];
        $this->district = [];
        $this->sector = [];
        $this->village = [];
    }
    public function newUserStaff(){
        $this->provinces = provinces::all();
    }
    public function delete($id){
        dd($id);
        $delete_business = user_business_information::where('user_id',$id)->delete();
        $delete_personal = user_personal_information::where('user_id',$id)->delete();
        $delete_user = User::where('id',$id)->delete();
        session()->flash('success', 'Successfully deleted user info.');
    }
    public function render()
    {
        if(!empty($this->province)) {
            $this->districts = district::where('province_id', $this->province)->get();
        }
        if(!empty($this->district)) {
            $this->sectors = sector::where('district_id', $this->district)->get();
        }
        if(!empty($this->sector)) {
            $this->cells = cell::where('sector_id', $this->sector)->get();
        }
        if(!empty($this->cell)) {
            $this->villages = village::where('cell_id', $this->cell)->get();
        }
        $this->all_members = User::orderBy('id','desc')->where('role','Staff')
            ->join('province', 'province.id', '=', 'users.province')
            ->join('district', 'district.id', '=', 'users.district')
            ->join('sector', 'sector.id', '=', 'users.sector')
            ->join('cell', 'cell.id', '=', 'users.cell')
            ->join('village', 'village.id', '=', 'users.village')
            ->select('users.*', 'province.name as provinceName','district.name as districtName','sector.name as sectorName','cell.name as cellName','village.name as villageName')
            ->get();
        return view('livewire.backend.admin.staff')->layout('Layouts.BackendMaster');
    }
}
