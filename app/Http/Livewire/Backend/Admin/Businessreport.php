<?php

namespace App\Http\Livewire\Backend\Admin;

use App\Models\businesType;
use App\Models\district;
use App\Models\User;
use App\Models\user_business_information;
use App\Models\user_personal_information;
use Livewire\Component;

class Businessreport extends Component
{
    public $businesstype_list;
    public $districts;
    public $RegisteredFrom;
    public $businessType;
    public $district;
    public $To;

    public function mount($RegisteredFrom=null, $To=null ,$district=null ,$businessType=null){
        $this->RegisteredFrom = $RegisteredFrom;
        $this->To = $To;
        $this->district = $district;
        $this->businessType = $businessType;
    }
    public function render()
    {
        $this->businesstype_list = businesType::all();
        $this->districts = district::all();
        $recent_user = User::orderBy('id','desc')->where('status','Approved')->get();
        foreach ($recent_user as $data){
            $ids = $data->id;
        }
        $ids= array();
        foreach($recent_user as $data){
            array_push($ids, $data->id);
        }
        $Approved_business_List = user_business_information::whereIn('user_id',$ids)
            ->join('province', 'province.id', '=', 'user_business_informations.businessProvince')
            ->join('district', 'district.id', '=', 'user_business_informations.businessDistrict')
            ->join('sector', 'sector.id', '=', 'user_business_informations.businessSector')
            ->join('cell', 'cell.id', '=', 'user_business_informations.businessCell')
            ->join('village', 'village.id', '=', 'user_business_informations.businessVillage')
            ->join('users', 'users.id', '=', 'user_business_informations.user_id')
            ->select('user_business_informations.*','users.name as userName', 'province.name as provinceName','district.name as districtName','sector.name as sectorName','cell.name as cellName','village.name as villageName')
            ->orderBy('id','desc');

        if($this->RegisteredFrom and $this->To){
            $Approved_business_List->whereBetween('user_business_informations.created_at',[$this->RegisteredFrom,$this->To]);
        }
        if($this->district){
            $Approved_business_List->where('user_business_informations.businessDistrict',$this->district);
        }
        if($this->businessType){
            $Approved_business_List->where('user_business_informations.businessType',$this->businessType);
        }
        return view('livewire.backend.admin.businessreport',['Approved_business_List' => $Approved_business_List->get()])->layout('Layouts.BackendMaster');
    }
}
