<?php

namespace App\Http\Livewire\Backend\Admin;

use App\Models\User;
use App\Models\user_business_information;
use App\Models\user_personal_information;
use Livewire\Component;

class Members extends Component
{
    public $all_members;
    public function render()
    {
        $recent_user = User::orderBy('id','desc')->get();
        foreach ($recent_user as $data){
            $ids = $data->id;
        }
        $ids= array();
        foreach($recent_user as $data){
            array_push($ids, $data->id);
        }
        $this->all_members = user_personal_information::whereIn('user_id',$ids)
            ->join('province', 'province.id', '=', 'user_personal_informations.province')
            ->join('district', 'district.id', '=', 'user_personal_informations.district')
            ->join('sector', 'sector.id', '=', 'user_personal_informations.sector')
            ->join('cell', 'cell.id', '=', 'user_personal_informations.cell')
            ->join('village', 'village.id', '=', 'user_personal_informations.village')
            ->join('users', 'users.id', '=', 'user_personal_informations.user_id')
            ->select('user_personal_informations.*','users.name as userName','users.gender','users.phone_number','users.email', 'province.name as provinceName','district.name as districtName','sector.name as sectorName','cell.name as cellName','village.name as villageName')
            ->orderBy('id','desc')
            ->get();
        return view('livewire.backend.admin.members')->layout('Layouts.BackendMaster');
    }
    public function deleteMember($id){
        $user_business = \App\Models\user_personal_information::where('id',$id)->first();
//        dd($user_business);
        $user_id = $user_business->user_id;
        $delete_business = user_business_information::where('user_id',$user_id)->delete();
        $delete_personal = user_personal_information::where('user_id',$id)->delete();
        $delete_user = User::where('id',$user_id)->delete();
        session()->flash('success', 'Successfully deleted user info.');
    }
}
