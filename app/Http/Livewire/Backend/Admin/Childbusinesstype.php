<?php

namespace App\Http\Livewire\Backend\Admin;

use App\Models\businesSubType;
use App\Models\businesType;
use App\Models\Childbusinesstypes;
use Livewire\Component;

class Childbusinesstype extends Component
{
    public $BusinessType;
    public $subBusinessType;
    public $ChildBusinessType = [];
    public $business_types_name;
    public $business_type_id;
    public $sub_business_type_id;
    public $sub_name;
    public $name;
    public $type_id;
    public $updateMode = false;
    public function submit()
    {
//        dd($this);
        $validatedData = $this->validate([
            'sub_name' => ['required'],
            'business_type_id' => ['required'],
            'sub_business_type_id' => ['required'],
        ]);
        $newData = Childbusinesstypes::create([
            'child_sub_name' => $this->sub_name,
//            'business_type_id' => $this->business_type_id,
            'sub_business_type_id' => $this->sub_business_type_id,
        ]);
        $this->clearForm();
        $this->emit('userRecord'); // Close model to using to jquery
        session()->flash('success', 'You have successfully created new record');
    }
    public function edit($id){
        $this->type_id = $id;
        $info = businesSubType::where('id',$id)->first();
        $this->name = $info->business_types_name;
    }
    public function update(){
//        dd($this);
        if ($this->type_id) {
            $update_type = \App\Models\businesSubType::find($this->type_id);
            $update_type->sub_name =  $this->sub_name;
            $update_type->save();
            $this->updateMode = false;
            session()->flash('success', 'Successfully Updated record.');
            $this->clearForm();
            $this->emit('userUpdate'); // Close model to using to jquery

        }
    }
    public function delete($id){
        $delete = businesType::where('id',$id)->delete();
        session()->flash('success', 'You have successfully created new record');
    }
    public function clearForm(){
        $this->business_types_name = '';
        $this->name = '';
        $this->sub_name = '';
    }
    public function render()
    {
        $this->BusinessType = businesType::all();
        if(!empty($this->business_type_id)) {
            $this->ChildBusinessType = businesSubType::where('business_type_id', $this->business_type_id)->get();
        }
        $this->subBusinessType = Childbusinesstypes::
        join('busines_sub_types', 'busines_sub_types.id', '=', 'childbusinesstypes.sub_business_type_id')
            ->select('childbusinesstypes.*','busines_sub_types.sub_name')
            ->get();
        return view('livewire.backend.admin.childbusinesstype')->layout('Layouts.BackendMaster');
    }
}
