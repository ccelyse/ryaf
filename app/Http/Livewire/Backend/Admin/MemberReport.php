<?php

namespace App\Http\Livewire\Backend\Admin;

use App\Models\businesType;
use App\Models\district;
use App\Models\User;
use App\Models\user_business_information;
use App\Models\user_personal_information;
use Livewire\Component;

class MemberReport extends Component
{
//    public $all_members;
    public $districts;
    public $businesstype_list;
    public $BornFrom;
    public $To;
    public $gender;
    public $district;
    public $businessType;

    public function mount($BornFrom=null, $To=null ,$gender=null ,$district=null ,$businessType=null){
        $this->BornFrom = $BornFrom;
        $this->To = $To;
        $this->gender = $gender;
        $this->district = $district;
        $this->businessType = $businessType;
    }
    public function render()
    {
        $this->districts = district::all();
        $this->businesstype_list = businesType::all();
        $recent_user = User::orderBy('id','desc')->get();
        foreach ($recent_user as $data){
            $ids = $data->id;
        }
        $ids= array();
        foreach($recent_user as $data){
            array_push($ids, $data->id);
        }
        $all_members = user_personal_information::whereIn('user_id',$ids)
            ->join('province', 'province.id', '=', 'user_personal_informations.province')
            ->join('district', 'district.id', '=', 'user_personal_informations.district')
            ->join('sector', 'sector.id', '=', 'user_personal_informations.sector')
            ->join('cell', 'cell.id', '=', 'user_personal_informations.cell')
            ->join('village', 'village.id', '=', 'user_personal_informations.village')
            ->join('users', 'users.id', '=', 'user_personal_informations.user_id')
            ->select('user_personal_informations.*','users.name as userName','users.gender','users.phone_number','users.email', 'province.name as provinceName','district.name as districtName','sector.name as sectorName','cell.name as cellName','village.name as villageName')
            ->orderBy('id','desc');

        if($this->BornFrom and $this->To){
            $all_members->whereBetween('user_personal_informations.created_at',[$this->BornFrom,$this->To]);
        }
        if($this->district){
            $all_members->where('user_personal_informations.district',$this->district);
        }
        return view('livewire.backend.admin.member-report',['all_members' => $all_members->get()])->layout('Layouts.BackendMaster');

    }
}
