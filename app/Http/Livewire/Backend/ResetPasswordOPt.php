<?php

namespace App\Http\Livewire\Backend;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Livewire\Component;

class ResetPasswordOPt extends Component
{
    public $phone_number;
    public $newpassword;
    public $opt_code = [];
    public $New_opt_code = [];
    public $AllowReset = [];
    public function submit(){
        $validatedData = $this->validate([
            'phone_number' => ['required'],
        ]);
        $check_number = User::where('phone_number', $this->phone_number)->value('id');
        if($check_number){
            $currentMonth = Carbon::now()->format('m');
            $code = mt_rand(10, 9999). $currentMonth;
            $message = "Your Opt code is $code , Verify and reset your password";
            $number_format = "25".$this->phone_number;
            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => 'https://api.mista.io/sms',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => array('to' => $number_format,'from' => 'RYAF','unicode' => '0','sms' => $message,'action' => 'send-sms'),
                CURLOPT_HTTPHEADER => array(
                    'x-api-key:1698cb44-330e-9b64-a160-c1d2dadf13c8-6e05ec6d'
                ),
            ));
            $response = curl_exec($curl);
            curl_close($curl);
            session()->flash('success', 'Verify the Opt code sent to your phone number and reset your password');
            $this->opt_code = $code;
        }else{
            session()->flash('success', 'Your phone number is not registered in our database');
        }
    }
    public function VerifyOpt(){
        if($this->New_opt_code == $this->opt_code){
           $update_password = User::where('phone_number',$this->phone_number)->update(['password'=>Hash::make($this->newpassword)]);
            session()->flash('success', 'Password is successfully updated');
        }else{
            session()->flash('success', 'Opt code is not matching');
        }
    }
    public function render()
    {
        return view('livewire.backend.reset-password-o-pt')->layout('Layouts.BackendMaster');
    }
}
