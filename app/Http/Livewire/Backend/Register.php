<?php

namespace App\Http\Livewire\Backend;

use App\Models\businesSubType;
use App\Models\businesType;
use App\Models\cell;
use App\Models\Childbusinesstypes;
use App\Models\district;
use App\Models\provinces;
use App\Models\sector;
use App\Models\User;
use App\Models\user_business_information;
use App\Models\user_personal_information;
use App\Models\village;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Livewire\Component;
use Illuminate\Support\Facades\Hash;

class Register extends Component
{
    public $currentStep = 1;
    public $user_id;
    public $maritalStatus;
    public $businesstype_list;
    public $position;
    public $nationaliDNo;
    public $dob;
    public $fieldStudied;
    public $LevelofStudy;
    public $subBusinessType_data = [];
    public $SubbusinessType = [];
    public $ChilbBusinessType = [];
    public $province;
    public $district;
    public $sector;
    public $cell;
    public $village;
    public $names;
    public $email;
    public $SubbusinessType_id;
    public $ChildbusinessType_id;
    public $ChildSubbusinessType_id;
    public $phonenumber;
    public $password;
    public $gender;
    public $provinces;
    public $Bprovinces;
    public $districts=[];
    public $Bdistricts=[];
    public $sectors=[];
    public $Bsectors=[];
    public $cells=[];
    public $Bcells=[];
    public $villages=[];
    public $Bvillages=[];
    public $businessName;
    public $business_level;
    public $businessCategory;
    public $registrationStatus;
    public $startYear;
    public $businessProvince;
    public $businessDistrict;
    public $businessSector;
    public $businessCell;
    public $businessVillage;
    public $businessType;

//    protected $rules = [
//        'names' => ['required'],
//        'phonenumber' => ['required'],
//        'gender' => ['required'],
//        'password' => ['required'],
//        'email' => ['required', 'max:255','unique:users']
//    ];

    public function firstStepSubmit()
    {
        $validatedData = $this->validate([
            'names' => ['required'],
            'phonenumber' => ['required'],
            'gender' => ['required'],
            'password' => ['required'],
//            'maritalStatus' => ['required'],
//            'position' => ['required'],
            'nationaliDNo' => ['required'],
            'dob' => ['required'],
            'fieldStudied' => ['required'],
            'LevelofStudy' => ['required'],
            'province' => ['required'],
            'district' => ['required'],
            'sector' => ['required'],
            'cell' => ['required'],
            'village' => ['required'],
            'email' => ['required', 'max:255','unique:users']
        ]);
//        dd($this);
        $this->currentStep = 2;
    }
    public function secondStepSubmit()
    {
        $validatedData = $this->validate([
            'businessName' => 'required',
            'business_level' => 'required',
            'businessCategory' => 'required',
            'registrationStatus' => 'required',
            'startYear' => 'required',
            'businessProvince' => 'required',
            'businessDistrict' => 'required',
            'businessSector' => 'required',
            'businessCell' => 'required',
            'businessVillage' => 'required',
            'businessType' => 'required',
        ]);

//        $this->currentStep = 3;
    }
    public function back($step)
    {
        $this->currentStep = $step;
    }

    public function mount()
    {
        $this->provinces = provinces::all();
        $this->Bprovinces = provinces::all();
//        $this->district = collect();
//        $this->sector = collect();
//        $this->cell = collect();
//        $this->village = collect();
    }
    public function submit(){

        $carbon_year = Carbon::now();
        $lastId = user_personal_information::all()->last()->id;
        $businessCode = "RYAF/".$carbon_year->year."/".$lastId;
        $newUser = User::create([
            'name' => $this->names,
            'email' => $this->email,
            'phone_number' => $this->phonenumber,
            'status' => "Pending",
            'role' => "Member",
            'gender' => $this->gender,
            'password' => Hash::make($this->password),
        ]);
        $add_details_personal = new user_personal_information();
        $add_details_personal->user_id = $newUser->id;
//        $add_details_personal->maritalStatus = $this->maritalStatus;
//        $add_details_personal->position = $this->position;
        $add_details_personal->nationaliDNo = $this->nationaliDNo;
        $add_details_personal->dob = $this->dob;
        $add_details_personal->fieldStudied = $this->fieldStudied;
        $add_details_personal->LevelofStudy = $this->LevelofStudy;
        $add_details_personal->province = $this->province;
        $add_details_personal->district = $this->district;
        $add_details_personal->sector = $this->sector;
        $add_details_personal->cell = $this->cell;
        $add_details_personal->village = $this->village;
        $add_details_personal->save();

        $add_business = new user_business_information();
        $add_business->user_id = $newUser->id;
        $add_business->businessName = $this->businessName;
        $add_business->business_level = $this->business_level;
        $add_business->businessCode = $businessCode;
        $add_business->businessCategory = $this->businessCategory;
        $add_business->registrationStatus = $this->registrationStatus;
        $add_business->startYear = $this->startYear;
        $add_business->businessProvince = $this->businessProvince;
        $add_business->businessDistrict = $this->businessDistrict;
        $add_business->businessSector = $this->businessSector;
        $add_business->businessCell = $this->businessCell;
        $add_business->businessVillage = $this->businessVillage;
        $add_business->businessVillage = $this->businessVillage;
        $add_business->businessType = $this->businessType;
        $add_business->SubbusinessType_id = $this->SubbusinessType_id;
        $add_business->ChildSubbusinessType_id = $this->ChildSubbusinessType_id;
        $add_business->save();
        $number_format = "25".$this->phonenumber;
        $message = "Dear $this->names \n You have successfully signed up! You will be notified when your account is approved";

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://api.mista.io/sms',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => array('to' => $number_format,'from' => 'RYAF','unicode' => '0','sms' => $message,'action' => 'send-sms'),
            CURLOPT_HTTPHEADER => array(
                'x-api-key:1698cb44-330e-9b64-a160-c1d2dadf13c8-6e05ec6d'
            ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $this->clearForm();

        $this->currentStep = 1;
        session()->flash('success', 'You have successfully signed up , you may login now!');
//        return redirect(url('/SignIn'));
    }
    public function clearForm()
    {
        $this->names = '';
        $this->email = '';
        $this->phonenumber = '';
        $this->gender = '';
        $this->password = '';
        $this->position = '';
        $this->nationaliDNo = '';
        $this->dob = '';
        $this->fieldStudied = '';
        $this->LevelofStudy = '';
        $this->province = '';
        $this->district = '';
        $this->sector = '';
        $this->village = '';
        $this->businessName = '';
        $this->businessCategory = '';
        $this->registrationStatus = '';
        $this->startYear = '';
        $this->businessProvince = '';
        $this->businessDistrict = '';
        $this->businessSector = '';
        $this->businessCell = '';
        $this->businessVillage = '';
        $this->businessType = '';
    }
    public function render()
    {
        if(!empty($this->province)) {
            $this->districts = district::where('province_id', $this->province)->get();
        }
        if(!empty($this->district)) {
            $this->sectors = sector::where('district_id', $this->district)->get();
        }
        if(!empty($this->sector)) {
            $this->cells = cell::where('sector_id', $this->sector)->get();
        }
        if(!empty($this->cell)) {
            $this->villages = village::where('cell_id', $this->cell)->get();
        }
        if(!empty($this->businessType)) {
            $this->subBusinessType_data = businesSubType::where('business_type_id', $this->businessType)->get();
        }
        if(!empty($this->SubbusinessType_id)) {
            $this->ChilbBusinessType = Childbusinesstypes::where('sub_business_type_id', $this->SubbusinessType_id)->get();
        }
        if(!empty($this->businessProvince)) {
            $this->Bdistricts = district::where('province_id', $this->businessProvince)->get();
        }
        if(!empty($this->businessDistrict)) {
            $this->Bsectors = sector::where('district_id', $this->businessDistrict)->get();
        }
        if(!empty($this->businessSector)) {
            $this->Bcells = cell::where('sector_id', $this->businessSector)->get();
        }
        if(!empty($this->businessCell)) {
            $this->Bvillages = village::where('cell_id', $this->businessCell)->get();
        }
        $this->businesstype_list = businesType::all();

        return view('livewire.backend.register')->layout('Layouts.BackendMaster');
    }
}
