<?php

namespace App\Http\Livewire\Backend;

use Livewire\Component;

class Certificate extends Component
{
    public function render()
    {
        return view('livewire.backend.certificate');
    }
}
