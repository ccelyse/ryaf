<?php

namespace App\Http\Livewire\Backend\Staff;

use App\Models\User;
use App\Models\user_business_information;
use Livewire\Component;

class Dashboard extends Component
{
    public $recent_business;
    public $all_members;
    public $all_Business;
    public $approvedMembers;
    public $pendingMembers;
    public $declinedMembers;
    public $user_id;
    public $businessName_Action;
    public $businessStatus = "Pending";
    public $updateMode = false;
    public function render()
    {
        $recent_user = User::orderBy('id','desc')->where('status','Pending')->limit('10')->get();
        foreach ($recent_user as $data){
            $ids = $data->id;
        }
        $ids= array();
        foreach($recent_user as $data){
            array_push($ids, $data->id);
        }
        $this->recent_business = user_business_information::whereIn('user_id',$ids)
            ->join('province', 'province.id', '=', 'user_business_informations.businessProvince')
            ->join('district', 'district.id', '=', 'user_business_informations.businessDistrict')
            ->join('sector', 'sector.id', '=', 'user_business_informations.businessSector')
            ->join('cell', 'cell.id', '=', 'user_business_informations.businessCell')
            ->join('village', 'village.id', '=', 'user_business_informations.businessVillage')
            ->select('user_business_informations.*', 'province.name as provinceName','district.name as districtName','sector.name as sectorName','cell.name as cellName','village.name as villageName')
            ->get();
        $this->all_members = User::where('role','Member')->count();
        $this->all_Business = user_business_information::count();
        $this->approvedMembers = User::where('status','Approved')->where('role','Member')->count();
        $this->pendingMembers = User::where('status','Pending')->where('role','Member')->count();
        $this->declinedMembers = User::where('status','Declined')->where('role','Member')->count();
        return view('livewire.backend.staff.dashboard')->layout('Layouts.BackendMaster');
    }
    public function actionBusiness($id)
    {
        $this->updateMode = true;
        $user_business = \App\Models\user_business_information::where('id',$id)->first();
//        dd($user_business);
        $this->user_id = $user_business->user_id;
        $this->businessName_Action = $user_business->businessName;

    }
    public function update(){
//        dd($this);
        if ($this->user_id) {
            $update_user = \App\Models\User::where('id',$this->user_id)->update(['status'=>$this->businessStatus]);
            $this->updateMode = false;
            session()->flash('success', 'Successfully Updated user info.');
            $this->resetInputFields();
            $this->emit('userUpdate'); // Close model to using to jquery

        }
    }
    private function resetInputFields(){
        $this->businessStatus = "Pending";
        $this->user_id = '';
        $this->businessName_Action = '';
    }
    public function cancel()
    {
        $this->updateMode = false;
        $this->resetInputFields();
    }
}
