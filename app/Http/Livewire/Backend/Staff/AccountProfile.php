<?php

namespace App\Http\Livewire\Backend\Staff;

use App\Models\cell;
use App\Models\district;
use App\Models\provinces;
use App\Models\sector;
use App\Models\User;
use App\Models\village;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Livewire\Component;

class AccountProfile extends Component
{
    public $names;
    public $email;
    public $phonenumber;
    public $nationalDNo;
    public $title;
    public $gender;
    public $provinces;
    public $districts=[];
    public $sectors=[];
    public $cells=[];
    public $villages=[];
    public $province;
    public $district;
    public $sector;
    public $cell;
    public $village;

    public function submit(){

        $update_account = User::where('id',auth()->id())->update(['name' => $this->names,'email' => $this->email,'phone_number' => $this->phonenumber,
            'gender' => $this->gender,'title' => $this->title,'nationalDNo' => $this->nationalDNo,'province' => $this->province,
            'district' => $this->district,'sector' => $this->sector,'cell' => $this->cell,'village' => $this->village]);
        session()->flash('success', 'You have successfully updated staff account');
//        return redirect(url('/SignIn'));
    }

    public function mount(){
        $this->provinces = provinces::all();
        $user_info = User::where('users.id',auth()->id())
            ->join('province', 'province.id', '=', 'users.province')
            ->join('district', 'district.id', '=', 'users.district')
            ->join('sector', 'sector.id', '=', 'users.sector')
            ->join('cell', 'cell.id', '=', 'users.cell')
            ->join('village', 'village.id', '=', 'users.village')
            ->select('users.*', 'province.name as provinceName','district.name as districtName','sector.name as sectorName','cell.name as cellName','village.name as villageName')
            ->first();
        $this->names = $user_info->name;
        $this->email = $user_info->email;
        $this->phonenumber = $user_info->phone_number;
        $this->nationalDNo = $user_info->nationalDNo;
        $this->title = $user_info->title;
        $this->gender = $user_info->gender;
        $this->province = $user_info->province;
        $this->district = $user_info->district;
        $this->sector = $user_info->sector;
        $this->cell = $user_info->cell;
        $this->village = $user_info->village;

        if(!empty($this->province)) {
            $this->districts = district::where('province_id', $this->province)->get();
        }
        if(!empty($this->district)) {
            $this->sectors = sector::where('district_id', $this->district)->get();
        }
        if(!empty($this->sector)) {
            $this->cells = cell::where('sector_id', $this->sector)->get();
        }
        if(!empty($this->cell)) {
            $this->villages = village::where('cell_id', $this->cell)->get();
        }
    }
    public function render()
    {
        return view('livewire.backend.staff.account-profile')->layout('Layouts.BackendMaster');
    }
}
