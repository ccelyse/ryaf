<?php

namespace App\Http\Livewire\Backend;

use Livewire\Component;

class Login extends Component
{
    public function render()
    {
        return view('livewire.backend.login')->layout('Layouts.BackendMaster');
    }
}
