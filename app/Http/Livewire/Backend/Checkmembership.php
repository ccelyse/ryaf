<?php

namespace App\Http\Livewire\Backend;

use App\Models\User;
use App\Models\user_business_information;
use Livewire\Component;

class Checkmembership extends Component
{
    public $membership_code;
    public $User_check = [];
    public $user_id;

    public function submit(){
        $check = user_business_information::where('businessCode', $this->membership_code)->first();
        $this->user_id  = $check->user_id;
        $this->User_check = $check;
        if(!empty($check)){
            session()->flash('success', 'You may download your certificate now!');
        }else{
            session()->flash('success', 'Sorry, Certificate with this code can not be found!');
        }
    }
    public function render()
    {
        return view('livewire.backend.checkmembership')->layout('Layouts.BackendMaster');
    }
}
