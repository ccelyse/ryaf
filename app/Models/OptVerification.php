<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OptVerification extends Model
{
    use HasFactory;
    protected $table = "opt_verifications";
    protected $fillable = ['opt_code','phone_number'];
}
