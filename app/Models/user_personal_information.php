<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class user_personal_information extends Model
{
    use HasFactory;
    protected $table = "user_personal_informations";
    protected $fillable = ['user_id','maritalStatus','position','nationaliDNo','dob','fieldStudied','LevelofStudy','province','district',
        'sector','cell','village'];
}
