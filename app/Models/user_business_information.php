<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class user_business_information extends Model
{
    use HasFactory;
    protected $table = "user_business_informations";
    protected $fillable = ['businessName','businessCode','businessCategory','registrationStatus','startYear','businessProvince','businessDistrict',
        'businessSector','businessCell','businessVillage','businessType','SubbusinessType_id','ChildSubbusinessType_id'];
}
