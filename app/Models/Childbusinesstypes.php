<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Childbusinesstypes extends Model
{
    use HasFactory;
    protected $table = "childbusinesstypes";
    protected $fillable = ['sub_business_type_id','child_sub_name'];
}
