<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class businesType extends Model
{
    use HasFactory;
    protected $table = "business_types";
    protected $fillable = ['business_types_name'];
}
