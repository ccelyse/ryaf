<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class businesSubType extends Model
{
    use HasFactory;
    protected $table = "busines_sub_types";
    protected $fillable = ['sub_name','business_type_id'];
}
