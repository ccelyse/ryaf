<?php


namespace App\Helpers;


use App\Models\Books;


class Cart
{
    public function __construct()
    {
        if($this->get() === null)
            $this->set($this->empty());
    }

    public function add(Books $books): void
    {
        $cart = $this->get();
        array_push($cart['books'], $books);
        $this->set($cart);
    }

    public function remove(int $bookId): void
    {
        $cart = $this->get();
        array_splice($cart['books'], array_search($bookId, array_column($cart['books'], 'id')), 1);
        $this->set($cart);
    }

    public function clear(): void
    {
        $this->set($this->empty());
    }

    public function empty(): array
    {
        return [
            'books' => [],
        ];
    }

    public function get(): ?array
    {
        return request()->session()->get('cart');
    }

    private function set($cart): void
    {
        request()->session()->put('cart', $cart);
    }
}
