<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('user')->group(function () {
    Route::post('/login', [\App\Http\Controllers\api\v1\LoginController::class, 'login']);
    Route::get('/AllBooks', [\App\Http\Controllers\api\v1\MobileController::class, 'AllBooks']);
    Route::post('/BookSubService', [\App\Http\Controllers\BackendController::class, 'BookSubService']);
    Route::post('/SubAccountInfo', [\App\Http\Controllers\BackendController::class, 'SubAccountInfo']);
    Route::post('/SuccessfulPayment', [\App\Http\Controllers\BackendController::class, 'SuccessfulPayment']);
    Route::post('/AuthorSuccessfulPayment', [\App\Http\Controllers\BackendController::class, 'AuthorSuccessfulPayment']);
    Route::get('/Country', [\App\Http\Controllers\api\v1\MobileController::class, 'Country']);
    Route::get('/Genre', [\App\Http\Controllers\api\v1\MobileController::class, 'Genre']);
    Route::get('/Authors', [\App\Http\Controllers\api\v1\MobileController::class, 'Authors']);
    Route::post('/register', [\App\Http\Controllers\api\v1\LoginController::class, 'register']);
    Route::middleware('auth:api')->post('/MyAccountDashboard', [\App\Http\Controllers\api\v1\MobileController::class, 'MyAccountDashboard']);
    Route::middleware('auth:api')->post('/MyAccount', [\App\Http\Controllers\api\v1\MobileController::class, 'MyAccount']);
    Route::middleware('auth:api')->post('/UpdateMyAccount', [\App\Http\Controllers\api\v1\MobileController::class, 'UpdateMyAccount']);
    Route::middleware('auth:api')->post('/ChangePassword', [\App\Http\Controllers\api\v1\MobileController::class, 'ChangePassword']);
    Route::middleware('auth:api')->post('/MyOrders', [\App\Http\Controllers\api\v1\MobileController::class, 'MyOrders']);
    Route::middleware('auth:api')->post('/MyBooks', [\App\Http\Controllers\api\v1\MobileController::class, 'MyBooks']);
    Route::middleware('auth:api')->post('/MyAddress', [\App\Http\Controllers\api\v1\MobileController::class, 'MyAddress']);
    Route::middleware('auth:api')->post('/AddAddress', [\App\Http\Controllers\api\v1\MobileController::class, 'AddAddress']);
    Route::middleware('auth:api')->post('/MakePrimary', [\App\Http\Controllers\api\v1\MobileController::class, 'MakePrimary']);
    Route::middleware('auth:api')->post('/removeAddress', [\App\Http\Controllers\api\v1\MobileController::class, 'removeAddress']);

    Route::post('/GenreBooks', [\App\Http\Controllers\MobileApi::class, 'GenreBooks']);
    Route::get('/FeaturedCategories', [\App\Http\Controllers\MobileApi::class, 'FeaturedCategories']);
//    Route::get('/AllBooks', [\App\Http\Controllers\MobileApi::class, 'AllBooks']);
    Route::get('/NewRelease', [\App\Http\Controllers\MobileApi::class, 'NewRelease']);
    Route::get('/PopularBooks', [\App\Http\Controllers\MobileApi::class, 'PopularBooks']);
    Route::post('/BookMore', [\App\Http\Controllers\MobileApi::class, 'BookMore']);
    Route::post('/AuthorProfile', [\App\Http\Controllers\MobileApi::class, 'AuthorProfile']);
});
