<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BackendController;
use App\Http\Controllers\FrontendController;
use App\Http\Livewire\HomeComponent;
use App\Http\Livewire\Frontend\Homepage;
use App\Http\Livewire\Frontend\Artists;
use App\Http\Livewire\Frontend\SignUp;
use App\Http\Livewire\Frontend\TrainingProgram;
use App\Http\Livewire\Frontend\Apply;
use App\Http\Livewire\Frontend\Login;
use App\Http\Livewire\Frontend\SignForArtists;
use App\Http\Livewire\Backend\Dashboard;
use App\Http\Livewire\Backend\Profile;
use App\Http\Livewire\Backend\EditProfile;
use App\Http\Livewire\Backend\BackendLogin;
use App\Http\Livewire\Backend\AdminUsers;
use App\Http\Livewire\Backend\AdminUserEdit;
use App\Http\Livewire\Backend\RegistrarDashboard;
use App\Http\Livewire\Backend\RegistrarCampus;
use App\Http\Livewire\Backend\RegistrarCampusEdit;
use App\Http\Livewire\Backend\RegistrarPeriod;
use App\Http\Livewire\Backend\RegistrarPeriodEdit;
use App\Http\Livewire\Backend\RegistrarCourses;
use App\Http\Livewire\Backend\RegistrarCoursesEdit;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//Route::get('/', Homepage::class);
Route::get('/', \App\Http\Livewire\Backend\Login::class);
Route::get('/SignIn', \App\Http\Livewire\Backend\Login::class);
Route::get('/SignUp', \App\Http\Livewire\Backend\Register::class);
Route::get('/OTPResetPassword', \App\Http\Livewire\Backend\ResetPasswordOPt::class);
Route::get('/CheckMembership', \App\Http\Livewire\Backend\Checkmembership::class);
Route::get('/DownloadCertificate/{id}', [BackendController::class, 'DownloadCertificate']);


Route::group(['middleware' => 'auth'], function () {
    Route::get('redirects', [BackendController::class, 'index']);

    /*ADMIN ROUTES*/
    Route::prefix('Admin')->group(function () {
        Route::get('/Dashboard', \App\Http\Livewire\Backend\Admin\Dashboard::class);
        Route::get('/ApprovedBusiness', \App\Http\Livewire\Backend\Admin\ApprovedBusiness::class);
        Route::get('/PendingBusiness', \App\Http\Livewire\Backend\Admin\PendingBusiness::class);
        Route::get('/DeclinedBusiness', \App\Http\Livewire\Backend\Admin\DeclinedBusiness::class);
        Route::get('/Members', \App\Http\Livewire\Backend\Admin\Members::class);
        Route::get('/Staff', \App\Http\Livewire\Backend\Admin\Staff::class);
        Route::get('/BusinessType', \App\Http\Livewire\Backend\Admin\Businesstype::class);
        Route::get('/SubBusinessType', \App\Http\Livewire\Backend\Admin\Subbusinesstype::class);
        Route::get('/ChildBusinessType', \App\Http\Livewire\Backend\Admin\Childbusinesstype::class);
    });

    /*STAFF ROUTES*/
    Route::prefix('Staff')->group(function () {
        Route::get('/Dashboard', \App\Http\Livewire\Backend\Staff\Dashboard::class);
        Route::get('/ApprovedBusiness', \App\Http\Livewire\Backend\Admin\ApprovedBusiness::class);
        Route::get('/PendingBusiness', \App\Http\Livewire\Backend\Admin\PendingBusiness::class);
        Route::get('/DeclinedBusiness', \App\Http\Livewire\Backend\Admin\DeclinedBusiness::class);
        Route::get('/Members', \App\Http\Livewire\Backend\Admin\Members::class);
        Route::get('/AccountProfile', \App\Http\Livewire\Backend\Staff\AccountProfile::class);
        Route::get('/MemberReport', \App\Http\Livewire\Backend\Admin\MemberReport::class);
        Route::get('/BusinessReport', \App\Http\Livewire\Backend\Admin\Businessreport::class);
    });

});

//Route::get('/register', [BackendController::class, 'login']);
