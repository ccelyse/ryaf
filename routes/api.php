<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(
    [
        'middleware' => 'auth:sanctum',
//        'prefix' => 'auth'
    ], function () {

    Route::post('/register', [\App\Http\Controllers\MobileApi::class, 'register']);
    Route::post('/UserLogin', [\App\Http\Controllers\MobileApi::class, 'UserLogin']);
    Route::get('/FeaturedCategories', [\App\Http\Controllers\MobileApi::class, 'FeaturedCategories']);

});

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});


//Route::get('/FeaturedCategories', [\App\Http\Controllers\MobileApi::class, 'FeaturedCategories']);
Route::post('/GenreBooks', [\App\Http\Controllers\MobileApi::class, 'GenreBooks']);
Route::get('/NewRelease', [\App\Http\Controllers\MobileApi::class, 'NewRelease']);
Route::get('/PopularBooks', [\App\Http\Controllers\MobileApi::class, 'PopularBooks']);
Route::post('/BookMore', [\App\Http\Controllers\MobileApi::class, 'BookMore']);
Route::post('/AuthorProfile', [\App\Http\Controllers\MobileApi::class, 'AuthorProfile']);
