<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>Card</title>
    <style>
        @page {
            margin: 0px;
        }
        body {
            margin: 0px;
        }
        .container{
            /*border: 3px solid green;*/
            /*padding: 100px;*/
        }
        .certificate{
            margin: 0;
            position: absolute;
            top: 50%;
            left: 50%;
            -ms-transform: translate(-50%, -50%);
            transform: translate(-50%, -50%);
            border: 3px solid green;
            padding: 10px;
            width: 90%;
        }
    </style>
</head>
<body class="" style="background: white; width: 100%; margin: 0;">
<div class="container">
    <div class="certificate">
        <p align="center">
            <img
                width="351"
                height="61"
                src="https://membershipportal.ryaf.rw/images/logo.png"
                alt="Logo"
            />
        </p>
        <h1 align="center">
            Membership Certificate
        </h1>
        <p align="center" style="font-size: 25px;">
            This to certify that <strong>{{$business_name}} </strong> is a Rwanda Youth in
            Agribusiness Forum (RYAF) Member since <strong> {{$new_date}}   </strong>year of Registration.
        </p>
        <p style="font-size: 20px;font-weight: bold;padding: 30px;">
            Date: {{$new_date_v1}}
        </p>
        <p style="font-size: 20px;font-weight: bold;padding: 30px;">
           Signed By :
        </p>
    </div>
</div>
</body>
</html>
