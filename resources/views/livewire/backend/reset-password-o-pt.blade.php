<div>
    <!--begin::Main-->
    <div class="d-flex flex-column flex-root">
        <!--begin::Login-->
        <div class="login login-1 login-signin-on d-flex flex-column flex-lg-row flex-column-fluid bg-white" id="kt_login">
            <!--begin::Aside-->
            <div class="login-aside d-flex flex-column flex-row-auto" style="background-color: #F2C98A;">
                <!--begin::Aside Top-->
                <div class="d-flex flex-column-auto flex-column pt-lg-40 pt-15">
                    <!--begin::Aside header-->
                    <a href="{{'/'}}" class="text-center mb-10">
                        <img src="{{ asset('backend/assets/media/logos/logo.png') }}" class="max-h-70px" alt="" />
                    </a>
                    <!--end::Aside header-->
                    <!--begin::Aside title-->
                    <h3 class="font-weight-bolder text-center font-size-h4 font-size-h1-lg" style="color: #619c41;">The Rwanda Youth in Agribusiness Forum
                        <br />(RYAF)
                    </h3>
                    <p style="color: #000; padding: 20px; font-size: 16px;">Being a RYAF member  will Belonging to a highly organized professional community of
                        like-minded young entrepreneurs which is well-connected and coordinated from grassroots to national level.</p>
                    <a href="{{'/CheckMembership'}}" id="kt_login_signup" class="text-primary font-weight-bolder" style="text-align: center; font-size: 16px;">Check membership certificate</a></span>
                    <!--end::Aside title-->
                </div>
                <!--end::Aside Top-->
                <!--begin::Aside Bottom-->
                <div class="aside-img d-flex flex-row-fluid bgi-no-repeat bgi-position-y-bottom bgi-position-x-center" style="background-image: url(https://preview.keenthemes.com/metronic/theme/html/demo2/dist/assets/media/svg/illustrations/login-visual-1.svg)"></div>
                <!--end::Aside Bottom-->
            </div>
            <!--begin::Aside-->
            <!--begin::Content-->
            <div class="login-content flex-row-fluid d-flex flex-column justify-content-center position-relative overflow-hidden p-7 mx-auto">
                <!--begin::Content body-->
                <div class="d-flex flex-column-fluid flex-center">
                    <!--begin::Signin-->
                    <div class="login-form login-signin">
                        @if(session('success'))
                            <div class="alert alert-success mb-4" role="alert" style="width: 100%;margin: 0 auto;">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
                                </button>
                                <strong>Success!</strong> {{ session('success') }}
                            </div>
                        @endif
                        <!--begin::Form-->
                        @if(empty($opt_code))
                        <form wire:submit.prevent="submit">
                        <!--begin::Title-->
                            <div class="pb-13 pt-lg-0 pt-5">
                                <h3 class="font-weight-bolder text-dark font-size-h4 font-size-h1-lg">Reset password</h3>
                                <span class="text-muted font-weight-bold font-size-h4">
									<a href="{{'/'}}" id="kt_login_signup" class="text-primary font-weight-bolder">Go to back to login</a>
                                </span>
                            </div>
                            <!--begin::Title-->
                            <!--begin::Form group-->
                            <div class="form-group">
                                <label class="font-size-h6 font-weight-bolder text-dark">Phone number</label>
                                <input oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                                       type = "number"
                                       maxlength = "10" class="form-control form-control-solid h-auto py-6 px-6 rounded-lg" type="number" wire:model="phone_number" autocomplete="off"  required/>
                                <span class="focus-border"></span>
                                @error('phone_number')
                                <strong>{{ $message }}</strong>
                                @enderror
                            </div>
                            <!--end::Form group-->
                            <!--begin::Action-->
                            <div class="pb-lg-0 pb-5">
                                <button type="submit" class="btn btn-primary font-weight-bolder font-size-h6 px-8 py-4 my-3 mr-3">
                                    <div wire:loading wire:target="submit">
                                        <img src="{{ asset('backend/assets/media/loading-gif-png-5.gif') }}" style="width: 30px;">
                                    </div>
                                    Send SMS code
                                </button>
                            </div>
                            <!--end::Action-->
                        </form>
                        @elseif($opt_code)
                        <form>
                            <!--begin::Title-->
                            <div class="pb-13 pt-lg-0 pt-5">
                                <h3 class="font-weight-bolder text-dark font-size-h4 font-size-h1-lg">Verify Opt code</h3>
                                <span class="text-muted font-weight-bold font-size-h4">
                                    <a href="{{'/'}}" id="kt_login_signup" class="text-primary font-weight-bolder">Go to back to login</a>
                                </span>
                            </div>
                            <!--begin::Title-->
                            <!--begin::Form group-->
                            <div class="form-group">
                                <label class="font-size-h6 font-weight-bolder text-dark">OPT code</label>
                                <input class="form-control form-control-solid h-auto py-6 px-6 rounded-lg" type="number" wire:model="New_opt_code" autocomplete="off"  required/>
                                <span class="focus-border"></span>
                                @error('New_opt_code')
                                <strong>{{ $message }}</strong>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label class="font-size-h6 font-weight-bolder text-dark">New Password</label>
                                <input class="form-control form-control-solid h-auto py-6 px-6 rounded-lg" type="password" wire:model="newpassword" autocomplete="off"  required/>
                                <span class="focus-border"></span>
                                @error('newpassword')
                                <strong>{{ $message }}</strong>
                                @enderror
                            </div>

                            <!--end::Form group-->
                            <!--begin::Action-->
                            <div class="pb-lg-0 pb-5">
                                <button wire:click.prevent="VerifyOpt()" class="btn btn-primary font-weight-bolder font-size-h6 px-8 py-4 my-3 mr-3">
                                    <div wire:loading wire:target="submit">
                                        <img src="{{ asset('backend/assets/media/loading-gif-png-5.gif') }}" style="width: 30px;">
                                    </div>
                                    Reset Password
                                </button>
                            </div>
                            <!--end::Action-->
                        </form>
                        @endif
                        <!--end::Form-->
                    </div>
                    <!--end::Signin-->
                </div>
                <!--end::Content body-->
                <!--begin::Content footer-->
                <div class="d-flex justify-content-lg-start justify-content-center align-items-end py-7 py-lg-0">
                    <div class="text-dark-50 font-size-lg font-weight-bolder mr-10">
                        <span class="mr-1">2021©</span>
                        <a href="#" target="_blank" class="text-dark-75 text-hover-primary">RYAF</a>
                    </div>
                    <a href="#" class="text-primary font-weight-bolder font-size-lg">Terms</a>
                    <a href="#" class="text-primary ml-5 font-weight-bolder font-size-lg">Contact Us</a>
                </div>
                <!--end::Content footer-->
            </div>
            <!--end::Content-->
        </div>
        <!--end::Login-->
    </div>
</div>
