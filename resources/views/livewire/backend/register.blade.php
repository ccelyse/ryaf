@push('styles')
    <style>
        @media (min-width: 992px){
            .login.login-1 .login-content {
                width: 100%;
                max-width: 100% !important;
            }
        }
        ul li{
            margin: 15px;
            color: #000;
        }
        .error{
            color: red !important;
        }
    </style>
@endpush
<div>
    <!--begin::Main-->
    <div class="d-flex flex-column flex-root">
        <!--begin::Login-->
        <div class="login login-1 login-signin-on d-flex flex-column flex-lg-row flex-column-fluid bg-white" id="kt_login">
            <!--begin::Aside-->
            <div class="login-aside d-flex flex-column flex-row-auto" style="background-color: #F2C98A;">
                <!--begin::Aside Top-->
                <div class="d-flex flex-column-auto flex-column pt-15">
                    <!--begin::Aside header-->
                    <a href="{{'/'}}" class="text-center mb-10">
                        <img src="{{ asset('backend/assets/media/logos/logo.png') }}" class="max-h-70px" alt="" />
                    </a>
                    <!--end::Aside header-->
                    <!--begin::Aside title-->
                    <h3 class="font-weight-bolder text-center font-size-h4 font-size-h1-lg" style="color: #619c41;">The Rwanda Youth in Agribusiness Forum
                        <br />(RYAF)</h3>
                    <p style="color: #000; padding: 20px; font-size: 16px;">Being a RYAF member  will Belonging to a highly organized professional community of
                        like-minded young entrepreneurs which is well-connected and coordinated from grassroots to national level.</p>
                    <h3 class="font-weight-bolder text-center font-size-h4 font-size-h1-lg" style="color: #619c41;">What are Member benefits to expect?</h3>
                    <ul>
                        <li>Belonging to a highly organized professional community of like-minded young agripreneurs which is well-connected and coordinated from grassroots to national level.</li>
                        <li>Opportunity to promote yourself and your achievements through RYAF networks</li>
                        <li>Easy access to global opportunities designed for youth in agribusiness.</li>
                        <li>Business Development Support</li>
                        <li>Mentoring and Coaching</li>
                        <li>Access to RYAF Capacity Building Programs</li>
                        <li>Access to markets for members through partner buyers or RYAF shops.</li>
                        <li>Face-to-face and online events</li>
                        <li>Latest industry news and thought leadership articles</li>
                        <li>Access to prestigious, exclusively negotiated benefits with hand-picked partners.</li>
                    </ul>
                    <!--end::Aside title-->
                </div>
                <!--end::Aside Top-->
                <!--begin::Aside Bottom-->
{{--                <div class="aside-img d-flex flex-row-fluid bgi-no-repeat bgi-position-y-bottom bgi-position-x-center" style="background-image: url(https://preview.keenthemes.com/metronic/theme/html/demo2/dist/assets/media/svg/illustrations/login-visual-1.svg)"></div>--}}
                <!--end::Aside Bottom-->
            </div>
            <!--begin::Aside-->
            <!--begin::Content-->
            <div class="login-content flex-row-fluid d-flex flex-column justify-content-center position-relative overflow-hidden p-7 mx-auto">
                <!--begin::Content body-->
                <div class="d-flex flex-column-fluid">
                    <!--begin::Container-->
                    <div class="container">
                        <div class="card card-custom">
                            <div class="card-body p-0">
                                <!--begin::Wizard-->
                                <div class="wizard wizard-1" id="kt_wizard" data-wizard-state="step-first" data-wizard-clickable="false">
                                    <!--begin::Wizard Nav-->
                                    <div class="wizard-nav border-bottom">
                                        <div class="wizard-steps p-8 p-lg-10">
                                            <!--begin::Wizard Step 1 Nav-->
                                            <div class="wizard-step" data-wizard-type="step" {{ $currentStep != 1 ? 'data-wizard-state=done' : 'data-wizard-state=current' }}>
                                                <div class="wizard-label">
                                                    <i class="wizard-icon flaticon-bus-stop"></i>
                                                    <h3 class="wizard-title">1. Personal Information</h3>
                                                </div>
                                                <span class="svg-icon svg-icon-xl wizard-arrow">
															<!--begin::Svg Icon | path:/metronic/theme/html/demo2/dist/assets/media/svg/icons/Navigation/Arrow-right.svg-->
															<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
																<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																	<polygon points="0 0 24 0 24 24 0 24" />
																	<rect fill="#000000" opacity="0.3" transform="translate(12.000000, 12.000000) rotate(-90.000000) translate(-12.000000, -12.000000)" x="11" y="5" width="2" height="14" rx="1" />
																	<path d="M9.70710318,15.7071045 C9.31657888,16.0976288 8.68341391,16.0976288 8.29288961,15.7071045 C7.90236532,15.3165802 7.90236532,14.6834152 8.29288961,14.2928909 L14.2928896,8.29289093 C14.6714686,7.914312 15.281055,7.90106637 15.675721,8.26284357 L21.675721,13.7628436 C22.08284,14.136036 22.1103429,14.7686034 21.7371505,15.1757223 C21.3639581,15.5828413 20.7313908,15.6103443 20.3242718,15.2371519 L15.0300721,10.3841355 L9.70710318,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.999999, 11.999997) scale(1, -1) rotate(90.000000) translate(-14.999999, -11.999997)" />
																</g>
															</svg>
                                                    <!--end::Svg Icon-->
														</span>
                                            </div>
                                            <!--end::Wizard Step 1 Nav-->
                                            <!--begin::Wizard Step 2 Nav-->
                                            <div class="wizard-step" data-wizard-type="step" {{ $currentStep != 2 ? 'data-wizard-state=pending' : 'data-wizard-state=current' }}>
                                                <div class="wizard-label">
                                                    <i class="wizard-icon flaticon-list"></i>
                                                    <h3 class="wizard-title">2. Business Information</h3>
                                                </div>
                                                <span class="svg-icon svg-icon-xl wizard-arrow">
															<!--begin::Svg Icon | path:/metronic/theme/html/demo2/dist/assets/media/svg/icons/Navigation/Arrow-right.svg-->
															<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
																<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																	<polygon points="0 0 24 0 24 24 0 24" />
																	<rect fill="#000000" opacity="0.3" transform="translate(12.000000, 12.000000) rotate(-90.000000) translate(-12.000000, -12.000000)" x="11" y="5" width="2" height="14" rx="1" />
																	<path d="M9.70710318,15.7071045 C9.31657888,16.0976288 8.68341391,16.0976288 8.29288961,15.7071045 C7.90236532,15.3165802 7.90236532,14.6834152 8.29288961,14.2928909 L14.2928896,8.29289093 C14.6714686,7.914312 15.281055,7.90106637 15.675721,8.26284357 L21.675721,13.7628436 C22.08284,14.136036 22.1103429,14.7686034 21.7371505,15.1757223 C21.3639581,15.5828413 20.7313908,15.6103443 20.3242718,15.2371519 L15.0300721,10.3841355 L9.70710318,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.999999, 11.999997) scale(1, -1) rotate(90.000000) translate(-14.999999, -11.999997)" />
																</g>
															</svg>
                                                    <!--end::Svg Icon-->
														</span>
                                            </div>
                                            <!--end::Wizard Step 2 Nav-->
                                        </div>
                                    </div>
                                    <!--end::Wizard Nav-->
                                    <!--begin::Wizard Body-->
                                    <div class="row justify-content-center my-10 px-8 my-lg-15 px-lg-10">
                                        <div class="col-xl-12">
                                            @if (session('success'))
                                                <div class="alert alert-primary mb-4" role="alert" style="margin: 0 auto;">
                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
                                                    </button>
                                                    <strong>Success!</strong> {{ session('success') }}
                                                    <a href="{{'SignIn'}}" class="btn btn-success font-weight-bolder text-uppercase px-9 py-4"><strong>SignIn!</strong></a>
                                                </div>
                                            @endif
                                            <!--begin::Wizard Form-->
                                            <form class="form" id="kt_form" wire:submit.prevent="submit">
                                                <!--begin::Wizard Step 1-->
                                                <div class="pb-5" data-wizard-type="step-content" {{ $currentStep != 1 ? '' : 'data-wizard-state=current' }}>
                                                    <h3 class="mb-10 font-weight-bold text-dark">Setup Your personal information</h3>
                                                    <!--begin::Input-->
                                                    <!-- <div class="form-group">
                                                        <label>Address Line 1</label>
                                                        <input type="text" class="form-control form-control-solid form-control-lg" name="address1" placeholder="Address Line 1" value="Address Line 1" />
                                                        <span class="form-text text-muted">Please enter your Address.</span>
                                                    </div> -->
                                                    <!--end::Input-->
                                                    <!--begin::Input-->
                                                    <div class="row">
                                                        <div class="col-xl-6">
                                                            <!--begin::Input-->
                                                            <div class="form-group">
                                                                <label>Names *</label>
                                                                <input type="text" wire:model="names" class="form-control form-control-solid form-control-lg" name="names" placeholder="Names"/>
                                                                <span class="form-text text-muted">Please enter your Names.</span>
                                                                @error('names') <span class="error">{{ $message }}</span> @enderror
                                                            </div>
                                                            <!--end::Input-->
                                                        </div>
                                                        <div class="col-xl-6">
                                                            <!--begin::Input-->
                                                            <div class="form-group">
                                                                <label>National ID No *</label>
                                                                <input type="number" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength = "16" wire:model="nationaliDNo" class="form-control form-control-solid form-control-lg" name="nationalid" placeholder="National ID No" />
                                                                <span class="form-text text-muted">Please enter your National ID No.</span>
                                                                @error('nationaliDNo') <span class="error">{{ $message }}</span> @enderror
                                                            </div>
                                                            <!--end::Input-->
                                                        </div>
{{--                                                        <div class="col-xl-6">--}}
{{--                                                            <!--begin::Input-->--}}
{{--                                                            <label>Marital Status</label>--}}
{{--                                                            <select name="maritalstatus" wire:model="maritalStatus" class="form-control form-control-solid form-control-lg">--}}
{{--                                                                <option selected="selected">Select Marital Status</option>--}}
{{--                                                                <option value="Single">Single</option>--}}
{{--                                                                <option value="Married">Married</option>--}}
{{--                                                                <option value="Widowed">Widowed</option>--}}
{{--                                                                <option value="Divorced">Divorced</option>--}}
{{--                                                            </select>--}}
{{--                                                            @error('maritalStatus') <span class="error">{{ $message }}</span> @enderror--}}
{{--                                                            <span class="form-text text-muted">Please select your Marital Status.</span>--}}
{{--                                                            <!--end::Input-->--}}
{{--                                                        </div>--}}
                                                    </div>
{{--                                                    <div class="row">--}}
{{--                                                        <div class="col-xl-6">--}}
{{--                                                            <!--begin::Input-->--}}
{{--                                                            <div class="form-group">--}}
{{--                                                                <label>Position</label>--}}
{{--                                                                <input type="text" wire:model="position" class="form-control form-control-solid form-control-lg" name="position" placeholder="Position" />--}}
{{--                                                                <span class="form-text text-muted">Please enter your Position.</span>--}}
{{--                                                                @error('position') <span class="error">{{ $message }}</span> @enderror--}}
{{--                                                            </div>--}}
{{--                                                            <!--end::Input-->--}}
{{--                                                        </div>--}}
{{--                                                        --}}
{{--                                                    </div>--}}
                                                    <div class="row">
                                                        <div class="col-xl-6">
                                                            <!--begin::Input-->
                                                            <div class="form-group">
                                                                <label>Gender *</label>
                                                                <select name="gender" wire:model="gender" class="form-control form-control-solid form-control-lg">
                                                                    <option selected="selected">Select Gender</option>
                                                                    <option value="Male">Male</option>
                                                                    <option value="Female">Female</option>
                                                                </select>
                                                                @error('gender') <span class="error">{{ $message }}</span> @enderror
                                                            </div>
                                                            <!--end::Input-->
                                                        </div>
                                                        <div class="col-xl-6">
                                                            <!--begin::Input-->
                                                            <div class="form-group">
                                                                <label>Date of Birth *</label>
                                                                <input type="date" wire:model="dob" class="form-control form-control-solid form-control-lg" name="dateofbirth" />
                                                                <span class="form-text text-muted">Please enter your Date of Birth</span>
                                                            </div>
                                                            <!--end::Input-->
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xl-6">
                                                            <!--begin::Input-->
                                                            <div class="form-group">
                                                                <label>E-mail *</label>
                                                                <input type="email" wire:model="email" class="form-control form-control-solid form-control-lg" name="email" placeholder="Email" />
                                                                <span class="form-text text-muted">Please enter your Email.</span>
                                                                @error('email') <span class="error">{{ $message }}</span> @enderror
                                                            </div>
                                                            <!--end::Input-->
                                                        </div>
                                                        <div class="col-xl-6">
                                                            <!--begin::Input-->
                                                            <div class="form-group">
                                                                <label>Phone Number *</label>
                                                                <input type="number" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength = "10" wire:model="phonenumber" class="form-control form-control-solid form-control-lg" name="phonenumber" placeholder="Phone Number" />
                                                                <span class="form-text text-muted">Please enter your Phone Number</span>
                                                                @error('phonenumber') <span class="error">{{ $message }}</span> @enderror
                                                            </div>
                                                            <!--end::Input-->
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xl-6">
                                                            <!--begin::Input-->
                                                            <div class="form-group">
                                                                <label>Field Studied *</label>
                                                                <input type="text" wire:model="fieldStudied" class="form-control form-control-solid form-control-lg" name="fieldstudied" placeholder="Field Studied" />
                                                                <span class="form-text text-muted">Please enter your Field Studied.</span>
                                                                @error('fieldStudied') <span class="error">{{ $message }}</span> @enderror
                                                            </div>
                                                            <!--end::Input-->
                                                        </div>
                                                        <div class="col-xl-6">
                                                            <!--begin::Input-->
                                                            <label>Level of Study *</label>
                                                            <select name="levelofstudy" wire:model="LevelofStudy"  class="form-control form-control-solid form-control-lg">
                                                                <option selected="selected">Select Level of Study</option>
                                                                <option value="Primary School">Primary School</option>
                                                                <option value="Tronc Commun(S3)">Tronc Commun(S3)</option>
                                                                <option value="Secondary School(A2)">Secondary School(A2)</option>
                                                                <option value="Secondary School(A2)">Advance diploma(A1)</option>
                                                                <option value="University(A0)">University(A0)</option>
                                                                <option value="Masters Degree">Masters Degree</option>
                                                                <option value="PhD">PhD</option>
                                                                <option value="N/A">N/A</option>
                                                            </select>
                                                            @error('LevelofStudy') <span class="error">{{ $message }}</span> @enderror
                                                            <!--end::Input-->
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xl-6">
                                                            <!--begin::Input-->
                                                            <div class="form-group">
                                                                <label>Province/Intara *</label>
                                                                <select name="province" wire:model="province" class="form-control form-control-solid form-control-lg">
                                                                    <option selected="selected">Select Province/Intara</option>
                                                                    @foreach($provinces as $province_)
                                                                        <option value={{ $province_->id }}>{{ $province_->name }}</option>
                                                                    @endforeach
                                                                </select>
                                                                @error('province') <span class="error">{{ $message }}</span> @enderror
                                                            </div>
                                                            <!--end::Input-->
                                                        </div>
                                                        @if(count($districts) > 0)
                                                            <div class="col-xl-6">
                                                                <!--begin::Input-->
                                                                <label>District/Akarere *</label>
                                                                <select name="district" wire:model="district" class="form-control form-control-solid form-control-lg">
                                                                    <option selected="selected">District/Akarere</option>
                                                                    @foreach($districts as $district_)
                                                                        <option value={{ $district_->id }}>{{ $district_->name }}</option>
                                                                    @endforeach
                                                                </select>
                                                                @error('district') <span class="error">{{ $message }}</span> @enderror
                                                                <!--end::Input-->
                                                            </div>
                                                        @endif
                                                    </div>
                                                    <div class="row">
                                                        @if(count($sectors) > 0)
                                                            <div class="col-xl-4">
                                                                <!--begin::Input-->
                                                                <div class="form-group">
                                                                    <label>Sector/Umurenge *</label>
                                                                    <select name="sector" wire:model="sector" class="form-control form-control-solid form-control-lg">
                                                                        <option selected="selected">Sector/Umurenge</option>
                                                                        @foreach($sectors as $sector_)
                                                                            <option value={{ $sector_->id }}>{{ $sector_->name }}</option>
                                                                        @endforeach
                                                                    </select>
                                                                    @error('sector') <span class="error">{{ $message }}</span> @enderror
                                                                </div>
                                                                <!--end::Input-->
                                                            </div>
                                                        @endif
                                                        @if(count($cells) > 0)
                                                            <div class="col-xl-4">
                                                                <!--begin::Input-->
                                                                <label>Cell/Akagari *</label>
                                                                <select name="cell" wire:model="cell" class="form-control form-control-solid form-control-lg">
                                                                    <option selected="selected">Cell/Akagari</option>
                                                                    @foreach($cells as $cells_)
                                                                        <option value={{ $cells_->id }}>{{ $cells_->name }}</option>
                                                                    @endforeach
                                                                </select>
                                                                @error('cell') <span class="error">{{ $message }}</span> @enderror
                                                                <!--end::Input-->
                                                            </div>
                                                        @endif
                                                        @if(count($villages) > 0)
                                                            <div class="col-xl-4">
                                                                <!--begin::Input-->
                                                                <label>Village/Umudugudu *</label>
                                                                <select name="village" wire:model="village" class="form-control form-control-solid form-control-lg">
                                                                    <option selected="selected">Village/Umudugudu</option>
                                                                    @foreach($villages as $village_)
                                                                        <option value={{ $village_->id }}>{{ $village_->name }}</option>
                                                                    @endforeach
                                                                </select>
                                                                @error('village') <span class="error">{{ $message }}</span> @enderror
                                                                <!--end::Input-->
                                                            </div>
                                                        @endif
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xl-6">
                                                            <!--begin::Input-->
                                                            <div class="form-group">
                                                                <label>Password *</label>
                                                                <input type="password" wire:model="password" class="form-control form-control-solid form-control-lg" placeholder="*******" />
                                                                <span class="form-text text-muted">Please enter your password.</span>
                                                                @error('password') <span class="error">{{ $message }}</span> @enderror
                                                            </div>
                                                            <!--end::Input-->
                                                        </div>
                                                        <div class="col-xl-6">
                                                            <!--begin::Input-->
                                                            <div class="form-group">
                                                                <label>Confirm Password *</label>
                                                                <input type="password" class="form-control form-control-solid form-control-lg" name="confirmpassword" placeholder="*******" />
                                                                <span class="form-text text-muted">Confirm password.</span>
                                                            </div>
                                                            <!--end::Input-->
                                                        </div>
                                                    </div>
                                                    <div class="d-flex justify-content-between border-top mt-5 pt-10">
                                                        <div>
                                                            {{--                                                        <button type="submit" class="btn btn-success font-weight-bolder text-uppercase px-9 py-4">Submit</button>--}}
                                                            <button type="button" class="btn btn-primary font-weight-bolder text-uppercase px-9 py-4" wire:click="firstStepSubmit" data-wizard-type="action-next">Next</button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--end::Wizard Step 1-->
                                                <!--begin::Wizard Step 2-->
                                                <div class="pb-5" data-wizard-type="step-content" {{ $currentStep != 2 ? 'data-wizard-state=pending' : 'data-wizard-state=current' }}>
                                                    <h4 class="mb-10 font-weight-bold text-dark">Enter Business Information</h4>
                                                    <!--begin::Input-->
                                                    <div class="row">
                                                        <div class="col-xl-6">
                                                            <!--begin::Input-->
                                                            <div class="form-group">
                                                                <label>Business Name *</label>
                                                                <input type="text" wire:model="businessName" class="form-control form-control-solid form-control-lg" name="businessname" placeholder="Business Name" />
                                                                <span class="form-text text-muted">Please enter your Business Name.</span>
                                                                @error('businessName') <span class="error">{{ $message }}</span> @enderror
                                                            </div>
                                                            <!--end::Input-->
                                                        </div>
                                                        <div class="col-xl-6">
                                                            <!--begin::Input-->
                                                            <label>Category *</label>
                                                            <select name="category" wire:model="businessCategory" class="form-control form-control-solid form-control-lg">
                                                                <option selected="selected">Select Category</option>
                                                                <option value="Company">Company</option>
                                                                <option value="Cooperative">Cooperative</option>
                                                                <option value="NGO">NGO</option>
                                                                <option value="Association">Association</option>
                                                                <option value="Other">Other</option>
                                                            </select>
                                                            <span class="form-text text-muted">Please select your category.</span>
                                                            @error('businessCategory') <span class="error">{{ $message }}</span> @enderror
                                                            <!--end::Input-->
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xl-6">
                                                            <!--begin::Input-->
                                                            <label>Business Level *</label>
                                                            <select name="category" wire:model="business_level" class="form-control form-control-solid form-control-lg">
                                                                <option selected="selected">Select Business level</option>
                                                                <option value="Startup">Startup</option>
                                                                <option value="survivor">survivor</option>
                                                                <option value="Maturity">Maturity</option>
                                                                <option value="NGO">Decline</option>
                                                            </select>
                                                            <span class="form-text text-muted">Please select your category.</span>
                                                            @error('business_level') <span class="error">{{ $message }}</span> @enderror
                                                        <!--end::Input-->
                                                        </div>
                                                        <div class="col-xl-6">
                                                            <!--begin::Input-->
                                                            <label>Registration Status *</label>
                                                            <select name="registrationstatus" wire:model="registrationStatus" class="form-control form-control-solid form-control-lg">
                                                                <option selected="selected">Select Registration Status</option>
                                                                <option value="Registered">Registered</option>
                                                                <option value="Not Registered">Not Registered</option>
                                                                <option value="In Process">In Process</option>
                                                            </select>
                                                            <span class="form-text text-muted">Please select your Registration Status.</span>
                                                            @error('registrationStatus') <span class="error">{{ $message }}</span> @enderror
                                                        <!--end::Input-->
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xl-6">
                                                            <!--begin::Input-->
                                                            <div class="form-group">
                                                                <label>Start Year *</label>
                                                                <input type="text" wire:model="startYear" class="form-control form-control-solid form-control-lg" name="startyear" placeholder="Start Year" />
                                                                <span class="form-text text-muted">Please enter your Start Year.</span>
                                                                @error('startYear') <span class="error">{{ $message }}</span> @enderror
                                                            </div>
                                                            <!--end::Input-->
                                                        </div>
                                                        <div class="col-xl-6">
                                                            <!--begin::Input-->
                                                            <div class="form-group">
                                                                <label>Province/Intara *</label>
                                                                <select name="province" wire:model="businessProvince" class="form-control form-control-solid form-control-lg">
                                                                    <option selected="selected">Select Province/Intara</option>
                                                                    @foreach($Bprovinces as $bprovince_)
                                                                        <option value={{ $bprovince_->id }}>{{ $bprovince_->name }}</option>
                                                                    @endforeach
                                                                </select>
                                                                @error('businessProvince') <span class="error">{{ $message }}</span> @enderror
                                                            </div>
                                                            <!--end::Input-->
                                                        </div>

                                                    </div>
                                                    <div class="row">
                                                        @if(count($Bdistricts) > 0)
                                                            <div class="col-xl-6">
                                                                <!--begin::Input-->
                                                                <label>District/Akarere *</label>
                                                                <select name="district" wire:model="businessDistrict" class="form-control form-control-solid form-control-lg">
                                                                    <option selected="selected">District/Akarere</option>
                                                                    @foreach($Bdistricts as $bdistrict_)
                                                                        <option value={{ $bdistrict_->id }}>{{ $bdistrict_->name }}</option>
                                                                    @endforeach
                                                                </select>
                                                                @error('businessDistrict') <span class="error">{{ $message }}</span> @enderror
                                                                <!--end::Input-->
                                                            </div>
                                                        @endif
                                                        @if(count($Bsectors) > 0)
                                                            <div class="col-xl-6">
                                                                <!--begin::Input-->
                                                                <div class="form-group">
                                                                    <label>Sector/Umurenge *</label>
                                                                    <select name="sector" wire:model="businessSector" class="form-control form-control-solid form-control-lg">
                                                                        <option selected="selected">Sector/Umurenge</option>
                                                                        @foreach($Bsectors as $bsector_)
                                                                            <option value={{ $bsector_->id }}>{{ $bsector_->name }}</option>
                                                                        @endforeach
                                                                    </select>
                                                                    @error('businessSector') <span class="error">{{ $message }}</span> @enderror
                                                                </div>
                                                                <!--end::Input-->
                                                            </div>
                                                        @endif
                                                    </div>
                                                    <div class="row">
                                                        @if(count($Bcells) > 0)
                                                            <div class="col-xl-6">
                                                                <!--begin::Input-->
                                                                <label>Cell/Akagari *</label>
                                                                <select name="cell" wire:model="businessCell" class="form-control form-control-solid form-control-lg">
                                                                    <option selected="selected">Cell/Akagari</option>
                                                                    @foreach($Bcells as $bcells_)
                                                                        <option value={{ $bcells_->id }}>{{ $bcells_->name }}</option>
                                                                    @endforeach

                                                                </select>
                                                                @error('businessCell') <span class="error">{{ $message }}</span> @enderror
                                                                <!--end::Input-->
                                                            </div>
                                                        @endif
                                                        @if(count($Bvillages) > 0)
                                                            <div class="col-xl-6">
                                                                <!--begin::Input-->
                                                                <label>Village/Umudugudu *</label>
                                                                <select name="village" wire:model="businessVillage" class="form-control form-control-solid form-control-lg">
                                                                    <option selected="selected">Village/Umudugudu</option>
                                                                    @foreach($Bvillages as $bvillage_)
                                                                        <option value={{ $bvillage_->id }}>{{ $bvillage_->name }}</option>
                                                                    @endforeach
                                                                </select>
                                                                @error('businessVillage') <span class="error">{{ $message }}</span> @enderror
                                                            <!--end::Input-->
                                                            </div>
                                                        @endif
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xl-6">
                                                            <!--begin::Input-->
                                                            <label>Business Type *</label>
                                                            <select name="cell" wire:model="businessType" class="form-control form-control-solid form-control-lg">
                                                                <option selected="selected">Select Business Type</option>
                                                                @foreach($businesstype_list as $type)
                                                                    <option value={{ $type->id }}>{{ $type->business_types_name }}</option>
                                                                @endforeach
                                                            </select>
                                                            @error('businessType') <span class="error">{{ $message }}</span> @enderror
                                                            <!--end::Input-->
                                                        </div>
                                                        @if(count($subBusinessType_data) > 0)
                                                            <div class="col-xl-6">
                                                                <!--begin::Input-->
                                                                <label>Sub Business Type </label>
                                                                <select name="village" wire:model="SubbusinessType_id" class="form-control form-control-solid form-control-lg">
                                                                    <option selected="selected">Select Sub Business Type</option>
                                                                    @foreach($subBusinessType_data as $subtype)
                                                                        <option value={{ $subtype->id }}>{{ $subtype->sub_name }}</option>
                                                                    @endforeach
                                                                </select>
                                                                @error('SubbusinessType_id') <span class="error">{{ $message }}</span> @enderror
                                                            <!--end::Input-->
                                                            </div>
                                                        @endif
                                                    </div>
                                                    <div class="row">
                                                            @if(count($ChilbBusinessType) > 0)
                                                                <div class="col-xl-6">
                                                                    <!--begin::Input-->
                                                                    <label>Child Business Type </label>
                                                                    <select name="village" wire:model="ChildSubbusinessType_id" class="form-control form-control-solid form-control-lg">
                                                                        <option selected="selected">Child Business Type</option>
                                                                        @foreach($ChilbBusinessType as $childtype)
                                                                            <option value={{ $childtype->id }}>{{ $childtype->child_sub_name }}</option>
                                                                        @endforeach
                                                                    </select>
                                                                    @error('ChildSubbusinessType_id') <span class="error">{{ $message }}</span> @enderror
                                                                <!--end::Input-->
                                                                </div>
                                                            @endif
                                                    </div>
                                                    <div class="d-flex justify-content-between border-top mt-5 pt-10">
                                                        <div class="mr-2">
                                                            <button type="button" wire:click="back(1)" class="btn btn-light-primary font-weight-bolder text-uppercase px-9 py-4">Previous</button>
                                                        </div>
                                                        <div>
                                                            <div wire:loading wire:target="submit">
                                                                <img src="https://www.ogulo.com/wp-content/themes/ogulo2/images/loader.gif" style="width: 50px">
                                                            </div>
                                                            <button wire:loading.remove wire:target="submit" type="submit" class="btn btn-success font-weight-bolder text-uppercase px-9 py-4">Submit</button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--end::Wizard Step 2-->
                                                <!--begin::Wizard Actions-->
{{--                                                <div class="d-flex justify-content-between border-top mt-5 pt-10">--}}
{{--                                                    <div class="mr-2">--}}
{{--                                                        <button type="button" class="btn btn-light-primary font-weight-bolder text-uppercase px-9 py-4" data-wizard-type="action-prev">Previous</button>--}}
{{--                                                    </div>--}}
{{--                                                    <div>--}}
{{--                                                        <button type="submit" class="btn btn-success font-weight-bolder text-uppercase px-9 py-4">Submit</button>--}}
{{--                                                        <button type="button" class="btn btn-primary font-weight-bolder text-uppercase px-9 py-4" data-wizard-type="action-next">Next</button>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
                                                <!--end::Wizard Actions-->
                                            </form>
                                            <!--end::Wizard Form-->
                                            @if (session('success'))
                                                <div class="alert alert-primary mb-4" role="alert" style="margin: 0 auto;">
                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
                                                    </button>
                                                    <strong>Success!</strong> {{ session('success') }}
                                                    <a href="{{'SignIn'}}" class="btn btn-success font-weight-bolder text-uppercase px-9 py-4"><strong>SignIn!</strong></a>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                    <!--end::Wizard Body-->
                                </div>
                                <!--end::Wizard-->
                            </div>
                            <!--end::Wizard-->
                        </div>
                    </div>
                    <!--end::Container-->
                </div>
                <!--end::Content body-->
                <!--begin::Content footer-->
                <div class="d-flex justify-content-lg-start justify-content-center align-items-end py-7 py-lg-0">
                    <div class="text-dark-50 font-size-lg font-weight-bolder mr-10">
                        <span class="mr-1">2021©</span>
                        <a href="#" target="_blank" class="text-dark-75 text-hover-primary">RYAF</a>
                    </div>
                    <a href="#" class="text-primary font-weight-bolder font-size-lg">Terms</a>
                    <a href="#" class="text-primary ml-5 font-weight-bolder font-size-lg">Contact Us</a>
                </div>
                <!--end::Content footer-->
            </div>
            <!--end::Content-->
        </div>
        <!--end::Login-->
    </div>

</div>
