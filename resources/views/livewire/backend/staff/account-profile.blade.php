@push('styles')
    <style>
        .buttons-print, .buttons-copy, .buttons-pdf{
            display: none !important;
        }
        .actions_buttons a{
            display: table-cell !important;
            position: relative !important;
        }
    </style>
@endpush
<div style="width: 100%;">
    <!--begin::Main-->
    <!--begin::Header Mobile-->
    <div id="kt_header_mobile" class="header-mobile">
        <!--begin::Logo-->
        <a href="{{'/Dashboard'}}">
            <img alt="Logo" src="{{'backend/assets/media/logos/logo-letter-1.png'}}" class="logo-default max-h-30px" />
        </a>
        <!--end::Logo-->
        <!--begin::Toolbar-->
        <div class="d-flex align-items-center">
            <button class="btn p-0 burger-icon burger-icon-left ml-4" id="kt_header_mobile_toggle">
                <span></span>
            </button>
            <button class="btn btn-icon btn-hover-transparent-white p-0 ml-3" id="kt_header_mobile_topbar_toggle">
					<span class="svg-icon svg-icon-xl">
						<!--begin::Svg Icon | path:/metronic/theme/html/demo2/dist/assets/media/svg/icons/General/User.svg-->
						<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
							<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
								<polygon points="0 0 24 0 24 24 0 24" />
								<path d="M12,11 C9.790861,11 8,9.209139 8,7 C8,4.790861 9.790861,3 12,3 C14.209139,3 16,4.790861 16,7 C16,9.209139 14.209139,11 12,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
								<path d="M3.00065168,20.1992055 C3.38825852,15.4265159 7.26191235,13 11.9833413,13 C16.7712164,13 20.7048837,15.2931929 20.9979143,20.2 C21.0095879,20.3954741 20.9979143,21 20.2466999,21 C16.541124,21 11.0347247,21 3.72750223,21 C3.47671215,21 2.97953825,20.45918 3.00065168,20.1992055 Z" fill="#000000" fill-rule="nonzero" />
							</g>
						</svg>
                        <!--end::Svg Icon-->
					</span>
            </button>
        </div>
        <!--end::Toolbar-->
    </div>
    <!--end::Header Mobile-->
    <div class="d-flex flex-column flex-root">
        <!--begin::Page-->
        <div class="d-flex flex-row flex-column-fluid page">
            <!--begin::Wrapper-->
            <div class="d-flex flex-column flex-row-fluid wrapper" id="kt_wrapper">
                <!--begin::Header-->
                <livewire:backend.header />
                <!--end::Header-->
                <!--begin::Content-->
                <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
                    <!--begin::Subheader-->
                    <div class="subheader py-2 py-lg-12 subheader-transparent" id="kt_subheader">
                        <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                            <!--begin::Info-->
                            <div class="d-flex align-items-center flex-wrap mr-1">
                                <!--begin::Heading-->
                                <div class="d-flex flex-column">
                                    <!--begin::Title-->
                                    <h2 class="text-white font-weight-bold my-2 mr-5">Account Profile</h2>
                                    <!--end::Title-->
                                </div>
                                <!--end::Heading-->
                            </div>
                            <!--end::Info-->
                        </div>
                    </div>
                    <!--end::Subheader-->
                    <!--begin::Entry-->
                    <div class="d-flex flex-column-fluid">
                        <!--begin::Container-->
                        <div class="container">
                            <!--begin::Card-->
                            <div class="card card-custom">
                                <div class="card-body">
                                    @if (session('success'))
                                        <div class="alert alert-success mb-4" role="alert" style="width: 100%;margin: 0 auto;">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
                                            </button>
                                            <strong>Success!</strong> {{ session('success') }}
                                        </div>
                                    @endif

                                    <form class="form" id="kt_form" wire:submit.prevent="submit">
                                        <div class="row">
                                            <div class="col-xl-6">
                                                <!--begin::Input-->
                                                <div class="form-group">
                                                    <label>Names</label>
                                                    <input type="text" wire:model="names" class="form-control form-control-solid form-control-lg" />
                                                </div>
                                                <!--end::Input-->
                                            </div>
                                            <div class="col-xl-6">
                                                <!--begin::Input-->
                                                <div class="form-group">
                                                    <label>E-mail</label>
                                                    <input type="email" wire:model="email" class="form-control form-control-solid form-control-lg" name="email" placeholder="Email" />
                                                    <span class="form-text text-muted">Please enter your Email.</span>
                                                </div>
                                                <!--end::Input-->
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xl-6">
                                                <!--begin::Input-->
                                                <div class="form-group">
                                                    <label>Phone Number</label>
                                                    <input type="number" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength = "10" wire:model="phonenumber" class="form-control form-control-solid form-control-lg" placeholder="Phone Number" />
                                                </div>
                                                <!--end::Input-->
                                            </div>
                                            <div class="col-xl-6">
                                                <!--begin::Input-->
                                                <div class="form-group">
                                                    <label>National ID No</label>
                                                    <input type="number" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength = "16" wire:model="nationalDNo" class="form-control form-control-solid form-control-lg" name="nationald" placeholder="National ID No" />
                                                </div>
                                                <!--end::Input-->
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xl-6">
                                                <!--begin::Input-->
                                                <div class="form-group">
                                                    <label>Title</label>
                                                    <input type="text" wire:model="title" class="form-control form-control-solid form-control-lg" name="Title" placeholder="Title" />
                                                </div>
                                                <!--end::Input-->
                                            </div>
                                            <div class="col-xl-6">
                                                <!--begin::Input-->
                                                <div class="form-group">
                                                    <label>Gender</label>
                                                    <select name="gender" wire:model="gender" class="form-control form-control-solid form-control-lg">
                                                        <option selected="selected">Select Gender</option>
                                                        <option value="Male">Male</option>
                                                        <option value="Female">Female</option>
                                                    </select>
                                                </div>
                                                <!--end::Input-->
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xl-6">
                                                <!--begin::Input-->
                                                <div class="form-group">
                                                    <label>Province/Intara</label>
                                                    <select name="province" wire:model="province" class="form-control form-control-solid form-control-lg">
                                                        <option selected="selected">Select Province/Intara</option>
                                                        @foreach($provinces as $province_)
                                                            <option value={{ $province_->id }}>{{ $province_->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <!--end::Input-->
                                            </div>
                                            <div class="col-xl-6">
                                                <!--begin::Input-->
                                                <label>District/Akarere</label>
                                                <select name="district" wire:model="district" class="form-control form-control-solid form-control-lg">
                                                    <option selected="selected">District/Akarere</option>
                                                    @foreach($districts as $district_)
                                                        <option value={{ $district_->id }}>{{ $district_->name }}</option>
                                                    @endforeach
                                                </select>
                                                @error('district') <span class="error">{{ $message }}</span> @enderror
                                            <!--end::Input-->
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xl-4">
                                                <!--begin::Input-->
                                                <div class="form-group">
                                                    <label>Sector/Umurenge</label>
                                                    <select name="sector" wire:model="sector" class="form-control form-control-solid form-control-lg">
                                                        <option selected="selected">Sector/Umurenge</option>
                                                        @foreach($sectors as $sector_)
                                                            <option value={{ $sector_->id }}>{{ $sector_->name }}</option>
                                                        @endforeach
                                                    </select>
                                                    @error('sector') <span class="error">{{ $message }}</span> @enderror
                                                </div>
                                                <!--end::Input-->
                                            </div>
                                            <div class="col-xl-4">
                                                <!--begin::Input-->
                                                <label>Cell/Akagari</label>
                                                <select name="cell" wire:model="cell" class="form-control form-control-solid form-control-lg">
                                                    <option selected="selected">Cell/Akagari</option>
                                                    @foreach($cells as $cells_)
                                                        <option value={{ $cells_->id }}>{{ $cells_->name }}</option>
                                                    @endforeach
                                                </select>
                                                @error('cell') <span class="error">{{ $message }}</span> @enderror
                                            <!--end::Input-->
                                            </div>
                                            <div class="col-xl-4">
                                                <!--begin::Input-->
                                                <label>Village/Umudugudu</label>
                                                <select name="village" wire:model="village" class="form-control form-control-solid form-control-lg">
                                                    <option selected="selected">Village/Umudugudu</option>
                                                    @foreach($villages as $village_)
                                                        <option value={{ $village_->id }}>{{ $village_->name }}</option>
                                                    @endforeach
                                                </select>
                                                @error('village') <span class="error">{{ $message }}</span> @enderror
                                            <!--end::Input-->
                                            </div>
                                        </div>
                                        <div class="d-flex justify-content-between border-top mt-5 pt-10">
                                            <div>
                                                <div wire:loading wire:target="submit">
                                                    <img src="https://www.ogulo.com/wp-content/themes/ogulo2/images/loader.gif" style="width: 50px">
                                                </div>
                                                <button wire:loading.remove wire:target="submit" type="submit" class="btn btn-success font-weight-bolder text-uppercase px-9 py-4">Update Account</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                            </div>
                            <!--end::Card-->
                        </div>
                        <!--end::Container-->
                    </div>
                    <!--end::Entry-->
                </div>
                <!--end::Content-->
                <!--begin::Footer-->
                <livewire:backend.footer />
                <!--end::Footer-->
            </div>
            <!--end::Wrapper-->
        </div>
        <!--end::Page-->
    </div>
    <!--end::Main-->
</div>
@push('scripts')
    <script type="text/javascript">
        window.livewire.on('userUpdate', () => {
            $('#newUserStaff').modal('hide');
        });
    </script>
@endpush
