<div style="width: 100%;">
    <!--begin::Main-->
    <!--begin::Header Mobile-->
    <div id="kt_header_mobile" class="header-mobile">
        <!--begin::Logo-->
        <a href="{{'Staff/Dashboard'}}">
            <img alt="Logo" src="{{'backend/assets/media/logos/logo-letter-1.png'}}" class="logo-default max-h-30px" />
        </a>
        <!--end::Logo-->
        <!--begin::Toolbar-->
        <div class="d-flex align-items-center">
            <button class="btn p-0 burger-icon burger-icon-left ml-4" id="kt_header_mobile_toggle">
                <span></span>
            </button>
            <button class="btn btn-icon btn-hover-transparent-white p-0 ml-3" id="kt_header_mobile_topbar_toggle">
					<span class="svg-icon svg-icon-xl">
						<!--begin::Svg Icon | path:/metronic/theme/html/demo2/dist/assets/media/svg/icons/General/User.svg-->
						<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
							<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
								<polygon points="0 0 24 0 24 24 0 24" />
								<path d="M12,11 C9.790861,11 8,9.209139 8,7 C8,4.790861 9.790861,3 12,3 C14.209139,3 16,4.790861 16,7 C16,9.209139 14.209139,11 12,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
								<path d="M3.00065168,20.1992055 C3.38825852,15.4265159 7.26191235,13 11.9833413,13 C16.7712164,13 20.7048837,15.2931929 20.9979143,20.2 C21.0095879,20.3954741 20.9979143,21 20.2466999,21 C16.541124,21 11.0347247,21 3.72750223,21 C3.47671215,21 2.97953825,20.45918 3.00065168,20.1992055 Z" fill="#000000" fill-rule="nonzero" />
							</g>
						</svg>
                        <!--end::Svg Icon-->
					</span>
            </button>
        </div>
        <!--end::Toolbar-->
    </div>
    <!--end::Header Mobile-->
    <div class="d-flex flex-column flex-root">
        <!--begin::Page-->
        <div class="d-flex flex-row flex-column-fluid page">
            <!--begin::Wrapper-->
            <div class="d-flex flex-column flex-row-fluid wrapper" id="kt_wrapper">
                <!--begin::Header-->
                <livewire:backend.header />
                <!--end::Header-->
                <!--begin::Content-->
                <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
                    <!--begin::Subheader-->
                    <div class="subheader py-2 py-lg-12 subheader-transparent" id="kt_subheader">
                        <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                            <!--begin::Info-->
                            <div class="d-flex align-items-center flex-wrap mr-1">
                                <!--begin::Heading-->
                                <div class="d-flex flex-column">
                                    <!--begin::Title-->
                                    <h2 class="text-white font-weight-bold my-2 mr-5">Dashboard</h2>
                                    <!--end::Title-->
                                    <!--begin::Breadcrumb-->
                                    <div class="d-flex align-items-center font-weight-bold my-2">
                                        <!--begin::Item-->
                                        <a href="#" class="opacity-75 hover-opacity-100">
                                            <i class="flaticon2-shelter text-white icon-1x"></i>
                                        </a>
                                        <!--end::Item-->
                                        <!--begin::Item-->
                                        <span class="label label-dot label-sm bg-white opacity-75 mx-3"></span>
                                        <a href="" class="text-white text-hover-white opacity-75 hover-opacity-100">Dashboard</a>
                                        <!--end::Item-->
                                    </div>
                                    <!--end::Breadcrumb-->
                                </div>
                                <!--end::Heading-->
                            </div>
                            <!--end::Info-->
                            <!--begin::Toolbar-->
                            <div class="d-flex align-items-center">
                                <!--begin::Button-->
                                <a href="#" class="btn btn-transparent-white font-weight-bold py-3 px-6 mr-2">Reports</a>
                                <!--end::Button-->
                                <!--begin::Dropdown-->
                                <div class="dropdown dropdown-inline ml-2" data-toggle="tooltip" title="Quick actions" data-placement="top">
                                    <a href="#" class="btn btn-white font-weight-bold py-3 px-6" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Actions</a>
                                </div>
                                <!--end::Dropdown-->
                            </div>
                            <!--end::Toolbar-->
                        </div>
                    </div>
                    <!--end::Subheader-->
                    <!--begin::Entry-->
                    <div class="d-flex flex-column-fluid">
                        <!--begin::Container-->
                        <div class="container">
                            <!--begin::Dashboard-->
                            <!--begin::Row-->
                            <div class="row">
                                <div class="col-xl-4">
                                    <!--begin::Tiles Widget 1-->
                                    <div class="card card-custom gutter-b card-stretch">
                                        <!--begin::Header-->
                                        <div class="card-header border-0 pt-5">
                                            <div class="card-title">
                                                <div class="card-label">
                                                    <div class="font-weight-bolder">RYAF Membership</div>
                                                    <div class="font-size-sm text-muted mt-2">{{$all_members}}</div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--end::Header-->
                                        <!--begin::Body-->
                                        <div class="card-body d-flex flex-column px-0">
                                            <!--begin::Items-->
                                            <div class="flex-grow-1 card-spacer-x">
                                                <!--begin::Item-->
                                                <div class="d-flex align-items-center justify-content-between mb-10">
                                                    <div class="d-flex align-items-center mr-2">
                                                        <div class="symbol symbol-50 symbol-light mr-3 flex-shrink-0">
                                                            <div class="symbol-label">
                                                                <img src="https://preview.keenthemes.com/metronic/theme/html/demo2/dist/assets/media/svg/misc/006-plurk.svg" alt="" class="h-50" />
                                                            </div>
                                                        </div>
                                                        <div>
                                                            <a href="{{'/Staff/ApprovedBusiness'}}" class="font-size-h6 text-dark-75 text-hover-primary font-weight-bolder">Approved Business</a>
                                                        </div>
                                                    </div>
                                                    <div class="label label-light label-inline font-weight-bold text-dark-50 py-4 px-3 font-size-base">+{{$approvedMembers}}</div>
                                                </div>
                                                <!--end::Item-->
                                                <!--begin::Item-->
                                                <div class="d-flex align-items-center justify-content-between mb-10">
                                                    <div class="d-flex align-items-center mr-2">
                                                        <div class="symbol symbol-50 symbol-light mr-3 flex-shrink-0">
                                                            <div class="symbol-label">
                                                                <img src="https://preview.keenthemes.com/metronic/theme/html/demo2/dist/assets/media/svg/misc/015-telegram.svg" alt="" class="h-50" />
                                                            </div>
                                                        </div>
                                                        <div>
                                                            <a href="{{'/Staff/PendingBusiness'}}" class="font-size-h6 text-dark-75 text-hover-primary font-weight-bolder">Pending Business</a>
                                                        </div>
                                                    </div>
                                                    <div class="label label-light label-inline font-weight-bold text-dark-50 py-4 px-3 font-size-base">+{{$pendingMembers}}</div>
                                                </div>
                                                <!--end::Item-->
                                                <!--begin::Item-->
                                                <div class="d-flex align-items-center justify-content-between">
                                                    <div class="d-flex align-items-center mr-2">
                                                        <div class="symbol symbol-50 symbol-light mr-3 flex-shrink-0">
                                                            <div class="symbol-label">
                                                                <img src="https://preview.keenthemes.com/metronic/theme/html/demo2/dist/assets/media/svg/misc/003-puzzle.svg" alt="" class="h-50" />
                                                            </div>
                                                        </div>
                                                        <div>
                                                            <a href="{{'/Staff/DeclinedBusiness'}}" class="font-size-h6 text-dark-75 text-hover-primary font-weight-bolder">Declined Business</a>
                                                        </div>
                                                    </div>
                                                    <div class="label label-light label-inline font-weight-bold text-dark-50 py-4 px-3 font-size-base">+{{$declinedMembers}}</div>
                                                </div>
                                                <!--end::Item-->
                                            </div>
                                            <!--end::Items-->
                                        </div>
                                        <!--end::Body-->
                                    </div>
                                    <!--end::Tiles Widget 1-->
                                </div>
                                <div class="col-xl-8">
                                    <div class="row">
                                        <div class="col-xl-6">
                                            <!--begin::Tiles Widget 11-->
                                            <div class="card card-custom bg-primary gutter-b" style="height: 150px">
                                                <div class="card-body">
																<span class="svg-icon svg-icon-3x svg-icon-white ml-n2">
																	<!--begin::Svg Icon | path:/metronic/theme/html/demo2/dist/assets/media/svg/icons/Layout/Layout-4-blocks.svg-->
																	<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
																		<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																			<rect x="0" y="0" width="24" height="24" />
																			<rect fill="#000000" x="4" y="4" width="7" height="7" rx="1.5" />
																			<path d="M5.5,13 L9.5,13 C10.3284271,13 11,13.6715729 11,14.5 L11,18.5 C11,19.3284271 10.3284271,20 9.5,20 L5.5,20 C4.67157288,20 4,19.3284271 4,18.5 L4,14.5 C4,13.6715729 4.67157288,13 5.5,13 Z M14.5,4 L18.5,4 C19.3284271,4 20,4.67157288 20,5.5 L20,9.5 C20,10.3284271 19.3284271,11 18.5,11 L14.5,11 C13.6715729,11 13,10.3284271 13,9.5 L13,5.5 C13,4.67157288 13.6715729,4 14.5,4 Z M14.5,13 L18.5,13 C19.3284271,13 20,13.6715729 20,14.5 L20,18.5 C20,19.3284271 19.3284271,20 18.5,20 L14.5,20 C13.6715729,20 13,19.3284271 13,18.5 L13,14.5 C13,13.6715729 13.6715729,13 14.5,13 Z" fill="#000000" opacity="0.3" />
																		</g>
																	</svg>
                                                                    <!--end::Svg Icon-->
																</span>
                                                    <div class="text-inverse-primary font-weight-bolder font-size-h2 mt-3">
                                                        {{$all_members}}
                                                    </div>
                                                    <a href="#" class="text-inverse-primary font-weight-bold font-size-lg mt-1">Members</a>
                                                </div>
                                            </div>
                                            <!--end::Tiles Widget 11-->
                                        </div>
                                        <div class="col-xl-6">
                                            <!--begin::Tiles Widget 12-->
                                            <div class="card card-custom gutter-b" style="height: 150px">
                                                <div class="card-body">
																<span class="svg-icon svg-icon-3x svg-icon-success">
																	<!--begin::Svg Icon | path:/metronic/theme/html/demo2/dist/assets/media/svg/icons/Communication/Group.svg-->
																	<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
																		<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																			<polygon points="0 0 24 0 24 24 0 24" />
																			<path d="M18,14 C16.3431458,14 15,12.6568542 15,11 C15,9.34314575 16.3431458,8 18,8 C19.6568542,8 21,9.34314575 21,11 C21,12.6568542 19.6568542,14 18,14 Z M9,11 C6.790861,11 5,9.209139 5,7 C5,4.790861 6.790861,3 9,3 C11.209139,3 13,4.790861 13,7 C13,9.209139 11.209139,11 9,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
																			<path d="M17.6011961,15.0006174 C21.0077043,15.0378534 23.7891749,16.7601418 23.9984937,20.4 C24.0069246,20.5466056 23.9984937,21 23.4559499,21 L19.6,21 C19.6,18.7490654 18.8562935,16.6718327 17.6011961,15.0006174 Z M0.00065168429,20.1992055 C0.388258525,15.4265159 4.26191235,13 8.98334134,13 C13.7712164,13 17.7048837,15.2931929 17.9979143,20.2 C18.0095879,20.3954741 17.9979143,21 17.2466999,21 C13.541124,21 8.03472472,21 0.727502227,21 C0.476712155,21 -0.0204617505,20.45918 0.00065168429,20.1992055 Z" fill="#000000" fill-rule="nonzero" />
																		</g>
																	</svg>
                                                                    <!--end::Svg Icon-->
																</span>
                                                    <div class="text-dark font-weight-bolder font-size-h2 mt-3">
                                                        {{$all_Business}}
                                                    </div>
                                                    <a href="#" class="text-muted text-hover-primary font-weight-bold font-size-lg mt-1">Business</a>
                                                </div>
                                            </div>
                                            <!--end::Tiles Widget 12-->
                                        </div>
                                        <div class="col-lg-12">
                                            <!--begin::Advance Table Widget 1-->
                                            <div class="card card-custom card-stretch gutter-b">
                                                <!--begin::Header-->
                                                <div class="card-header border-0 py-5">
                                                    <h3 class="card-title align-items-start flex-column">
                                                        <span class="card-label font-weight-bolder text-dark">Recent business registration</span>
                                                    </h3>
                                                    @if (session('success'))
                                                        <div class="alert alert-success mb-4" role="alert" style="width: 100%;margin: 0 auto;">
                                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
                                                            </button>
                                                            <strong>Success!</strong> {{ session('success') }}
                                                        </div>
                                                    @endif
                                                </div>
                                                <!--end::Header-->
                                                <!--begin::Body-->
                                                <div class="card-body py-0">
                                                    <!--begin::Table-->
                                                    <div class="table-responsive">
                                                        <table class="table table-head-custom table-vertical-center" id="kt_advance_table_widget_1">
                                                            <thead>
                                                            <tr class="text-left">
                                                                <th class="pl-0" style="width: 20px">
                                                                    <label class="checkbox checkbox-lg checkbox-inline">
                                                                        <input type="checkbox" value="1" />
                                                                        <span></span>
                                                                    </label>
                                                                </th>
                                                                <th style="min-width: 150px">Business Name</th>
                                                                <th class="pr-0">Status</th>
                                                                <th style="min-width: 150px">Location</th>
                                                                <th class="pr-0 text-right" style="min-width: 150px">action</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            @foreach($recent_business as $business)
                                                                <tr>

                                                                    <td class="pl-0">
                                                                        <label class="checkbox checkbox-lg checkbox-inline">
                                                                            <input type="checkbox" value="1" />
                                                                            <span></span>
                                                                        </label>
                                                                    </td>
                                                                    <td class="pl-0">
                                                                        <a href="#" class="text-dark-75 font-weight-bolder text-hover-primary mb-1 font-size-lg">{{$business->businessName}}</a>
                                                                        <span class="text-muted font-weight-bold text-muted d-block">{{$business->businessCategory}}</span>
                                                                        <span class="text-muted font-weight-bold text-muted d-block">{{$business->registrationStatus}}</span>
                                                                        <span class="text-muted font-weight-bold text-muted d-block">{{$business->startYear}}</span>
                                                                    </td>
                                                                    <td>
                                                                        <a href="#" class="text-dark-75 font-weight-bolder text-hover-primary mb-1 font-size-lg">Pending</a>
                                                                    </td>
                                                                    <td>
                                                                        <a href="#" class="text-dark-75 font-weight-bolder text-hover-primary mb-1 font-size-lg">{{$business->provinceName}}</a>
                                                                        <span class="text-muted font-weight-bold text-muted d-block">{{$business->districtName}}</span>
                                                                        <span class="text-muted font-weight-bold text-muted d-block">{{$business->sectorName}}</span>
                                                                        <span class="text-muted font-weight-bold text-muted d-block">{{$business->cellName}}</span>
                                                                        <span class="text-muted font-weight-bold text-muted d-block">{{$business->villageName}}</span>
                                                                    </td>
                                                                    <td class="pr-0 text-right">
                                                                        <a data-toggle="modal" wire:click="actionBusiness({{ $business->id }})" data-target="#UpdateBusinessStatus" class="btn btn-icon btn-light btn-hover-primary btn-sm mx-3">
																		<span class="svg-icon svg-icon-md svg-icon-primary">
																			<!--begin::Svg Icon | path:/metronic/theme/html/demo2/dist/assets/media/svg/icons/Communication/Write.svg-->
																			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
																				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																					<rect x="0" y="0" width="24" height="24" />
																					<path d="M12.2674799,18.2323597 L12.0084872,5.45852451 C12.0004303,5.06114792 12.1504154,4.6768183 12.4255037,4.38993949 L15.0030167,1.70195304 L17.5910752,4.40093695 C17.8599071,4.6812911 18.0095067,5.05499603 18.0083938,5.44341307 L17.9718262,18.2062508 C17.9694575,19.0329966 17.2985816,19.701953 16.4718324,19.701953 L13.7671717,19.701953 C12.9505952,19.701953 12.2840328,19.0487684 12.2674799,18.2323597 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.701953, 10.701953) rotate(-135.000000) translate(-14.701953, -10.701953)" />
																					<path d="M12.9,2 C13.4522847,2 13.9,2.44771525 13.9,3 C13.9,3.55228475 13.4522847,4 12.9,4 L6,4 C4.8954305,4 4,4.8954305 4,6 L4,18 C4,19.1045695 4.8954305,20 6,20 L18,20 C19.1045695,20 20,19.1045695 20,18 L20,13 C20,12.4477153 20.4477153,12 21,12 C21.5522847,12 22,12.4477153 22,13 L22,18 C22,20.209139 20.209139,22 18,22 L6,22 C3.790861,22 2,20.209139 2,18 L2,6 C2,3.790861 3.790861,2 6,2 L12.9,2 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
																				</g>
																			</svg>
                                                                            <!--end::Svg Icon-->
																		</span>
                                                                        </a>
                                                                        <a href="#" class="btn btn-icon btn-light btn-hover-primary btn-sm">
																		<span class="svg-icon svg-icon-md svg-icon-primary">
																			<!--begin::Svg Icon | path:/metronic/theme/html/demo2/dist/assets/media/svg/icons/General/Trash.svg-->
																			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
																				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																					<rect x="0" y="0" width="24" height="24" />
																					<path d="M6,8 L6,20.5 C6,21.3284271 6.67157288,22 7.5,22 L16.5,22 C17.3284271,22 18,21.3284271 18,20.5 L18,8 L6,8 Z" fill="#000000" fill-rule="nonzero" />
																					<path d="M14,4.5 L14,4 C14,3.44771525 13.5522847,3 13,3 L11,3 C10.4477153,3 10,3.44771525 10,4 L10,4.5 L5.5,4.5 C5.22385763,4.5 5,4.72385763 5,5 L5,5.5 C5,5.77614237 5.22385763,6 5.5,6 L18.5,6 C18.7761424,6 19,5.77614237 19,5.5 L19,5 C19,4.72385763 18.7761424,4.5 18.5,4.5 L14,4.5 Z" fill="#000000" opacity="0.3" />
																				</g>
																			</svg>
                                                                            <!--end::Svg Icon-->
																		</span>
                                                                        </a>
                                                                    </td>
                                                                </tr>
                                                            @endforeach
                                                            </tbody>
                                                        </table>
                                                        <!-- Modal-->
                                                        <div class="modal fade" wire:ignore.self id="UpdateBusinessStatus" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
                                                            <div class="modal-dialog modal-dialog-centered" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title">Update status of : {{$businessName_Action}}</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <i aria-hidden="true" class="ki ki-close"></i>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <div class="row">
                                                                            <div class="col-xl-12">
                                                                                <!--begin::Input-->
                                                                                <label>Status</label>
                                                                                <select wire:model="businessStatus"  class="form-control form-control-solid form-control-lg">
                                                                                    <option>Select status</option>
                                                                                    <option selected="Approved">Approved</option>
                                                                                    <option value="Declined">Declined</option>
                                                                                </select>
                                                                                <input type="text" wire:model="user_id" class="form-control form-control-solid form-control-lg" hidden />
                                                                                <!--end::Input-->
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button wire:click.prevent="cancel()"  class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                                        <button wire:click.prevent="update()" class="btn btn-primary">Save changes</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <!--end::Table-->
                                                </div>
                                                <!--end::Body-->
                                            </div>
                                            <!--end::Advance Table Widget 1-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--end::Row-->
                            <!--end::Dashboard-->
                        </div>
                        <!--end::Container-->
                    </div>
                    <!--end::Entry-->
                </div>
                <!--end::Content-->
                <!--begin::Footer-->
                <livewire:backend.footer />
                <!--end::Footer-->
            </div>
            <!--end::Wrapper-->
        </div>
        <!--end::Page-->
    </div>
    <!--end::Main-->
</div>
@push('scripts')
    <script type="text/javascript">
        window.livewire.on('userUpdate', () => {
            $('#UpdateBusinessStatus').modal('hide');
        });
    </script>
@endpush
