@push('styles')
    <style>
        .buttons-print, .buttons-copy, .buttons-pdf{
            display: none !important;
        }
        .actions_buttons a{
            display: table-cell !important;
            position: relative !important;
        }
    </style>
@endpush
<div style="width: 100%;">
    <!--begin::Main-->
    <!--begin::Header Mobile-->
    <div id="kt_header_mobile" class="header-mobile">
        <!--begin::Logo-->
        <a href="{{'/Dashboard'}}">
            <img alt="Logo" src="{{'backend/assets/media/logos/logo-letter-1.png'}}" class="logo-default max-h-30px" />
        </a>
        <!--end::Logo-->
        <!--begin::Toolbar-->
        <div class="d-flex align-items-center">
            <button class="btn p-0 burger-icon burger-icon-left ml-4" id="kt_header_mobile_toggle">
                <span></span>
            </button>
            <button class="btn btn-icon btn-hover-transparent-white p-0 ml-3" id="kt_header_mobile_topbar_toggle">
					<span class="svg-icon svg-icon-xl">
						<!--begin::Svg Icon | path:/metronic/theme/html/demo2/dist/assets/media/svg/icons/General/User.svg-->
						<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
							<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
								<polygon points="0 0 24 0 24 24 0 24" />
								<path d="M12,11 C9.790861,11 8,9.209139 8,7 C8,4.790861 9.790861,3 12,3 C14.209139,3 16,4.790861 16,7 C16,9.209139 14.209139,11 12,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
								<path d="M3.00065168,20.1992055 C3.38825852,15.4265159 7.26191235,13 11.9833413,13 C16.7712164,13 20.7048837,15.2931929 20.9979143,20.2 C21.0095879,20.3954741 20.9979143,21 20.2466999,21 C16.541124,21 11.0347247,21 3.72750223,21 C3.47671215,21 2.97953825,20.45918 3.00065168,20.1992055 Z" fill="#000000" fill-rule="nonzero" />
							</g>
						</svg>
                        <!--end::Svg Icon-->
					</span>
            </button>
        </div>
        <!--end::Toolbar-->
    </div>
    <!--end::Header Mobile-->
    <div class="d-flex flex-column flex-root">
        <!--begin::Page-->
        <div class="d-flex flex-row flex-column-fluid page">
            <!--begin::Wrapper-->
            <div class="d-flex flex-column flex-row-fluid wrapper" id="kt_wrapper">
                <!--begin::Header-->
                <livewire:backend.header />
                <!--end::Header-->
                <!--begin::Content-->
                <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
                    <!--begin::Subheader-->
                    <div class="subheader py-2 py-lg-12 subheader-transparent" id="kt_subheader">
                        <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                            <!--begin::Info-->
                            <div class="d-flex align-items-center flex-wrap mr-1">
                                <!--begin::Heading-->
                                <div class="d-flex flex-column">
                                    <!--begin::Title-->
                                    <h2 class="text-white font-weight-bold my-2 mr-5">Members</h2>
                                    <!--end::Title-->
                                </div>
                                <!--end::Heading-->
                            </div>
                            <!--end::Info-->
                        </div>
                    </div>
                    <!--end::Subheader-->
                    <!--begin::Entry-->
                    <div class="d-flex flex-column-fluid">
                        <!--begin::Container-->
                        <div class="container">
                            <!--begin::Card-->
                            <div class="card card-custom">
                                <div class="card-header flex-wrap border-0 pt-6 pb-0">

                                    <div class="card-toolbar">
                                        <!--begin::Button-->
                                        <a data-toggle="modal" wire:click="newUserStaff()" data-target="#newUserStaff" class="btn btn-primary font-weight-bolder">
											<span class="svg-icon svg-icon-md">
												<!--begin::Svg Icon | path:/metronic/theme/html/demo2/dist/assets/media/svg/icons/Design/Flatten.svg-->
												<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
													<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
														<rect x="0" y="0" width="24" height="24" />
														<circle fill="#000000" cx="9" cy="15" r="6" />
														<path d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z" fill="#000000" opacity="0.3" />
													</g>
												</svg>
                                                <!--end::Svg Icon-->
											</span>New Record</a>
                                        <!--end::Button-->
                                    </div>
                                </div>
                                <div class="card-body">
                                    @if (session('success'))
                                        <div class="alert alert-success mb-4" role="alert" style="width: 100%;margin: 0 auto;">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
                                            </button>
                                            <strong>Success!</strong> {{ session('success') }}
                                        </div>
                                @endif
                                <!--begin: Datatable-->
                                    <table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
                                        <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Gender</th>
                                            <th>Title</th>
                                            <th>ID No</th>
                                            <th>E-mail</th>
                                            <th>Telephone</th>
                                            <th>Province</th>
                                            <th>District</th>
                                            <th>Sector</th>
                                            <th>Cell</th>
                                            <th>Village</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($all_members as $members)
                                            <tr>
                                                <td>{{$members->name}}</td>
                                                <td>{{$members->gender}}</td>
                                                <td>{{$members->title}}</td>
                                                <td>{{$members->nationalDNo}}</td>
                                                <td>{{$members->email}}</td>
                                                <td>{{$members->phone_number}}</td>
                                                <td>{{$members->provinceName}}</td>
                                                <td>{{$members->districtName}}</td>
                                                <td>{{$members->sectorName}}</td>
                                                <td>{{$members->cellName}}</td>
                                                <td>{{$members->villageName}}</td>
                                                <td class="actions_buttons">
                                                    <a wire:click="delete({{ $members->id }})" onclick="return confirm('Are you sure you would like to delete record?');" class="btn btn-icon btn-light btn-hover-primary btn-sm">
                                                        <span class="svg-icon svg-icon-md svg-icon-primary">
                                                            <!--begin::Svg Icon | path:/metronic/theme/html/demo2/dist/assets/media/svg/icons/General/Trash.svg-->
                                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                    <rect x="0" y="0" width="24" height="24" />
                                                                    <path d="M6,8 L6,20.5 C6,21.3284271 6.67157288,22 7.5,22 L16.5,22 C17.3284271,22 18,21.3284271 18,20.5 L18,8 L6,8 Z" fill="#000000" fill-rule="nonzero" />
                                                                    <path d="M14,4.5 L14,4 C14,3.44771525 13.5522847,3 13,3 L11,3 C10.4477153,3 10,3.44771525 10,4 L10,4.5 L5.5,4.5 C5.22385763,4.5 5,4.72385763 5,5 L5,5.5 C5,5.77614237 5.22385763,6 5.5,6 L18.5,6 C18.7761424,6 19,5.77614237 19,5.5 L19,5 C19,4.72385763 18.7761424,4.5 18.5,4.5 L14,4.5 Z" fill="#000000" opacity="0.3" />
                                                                </g>
                                                            </svg>
                                                            <!--end::Svg Icon-->
                                                        </span>
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                        <!--end: Datatable-->
                                </div>
                                <div class="modal fade" wire:ignore.self id="newUserStaff" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title">Add New Staff</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <i aria-hidden="true" class="ki ki-close"></i>
                                                </button>
                                            </div>
                                            <form class="form" id="kt_form" wire:submit.prevent="submit">
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <div class="col-xl-6">
                                                            <!--begin::Input-->
                                                            <div class="form-group">
                                                                <label>Names</label>
                                                                <input type="text" wire:model="names" class="form-control form-control-solid form-control-lg" name="names" placeholder="Names" />
                                                                <span class="form-text text-muted">Please enter your Names.</span>
                                                                @error('names') <span class="error">{{ $message }}</span> @enderror
                                                            </div>
                                                            <!--end::Input-->
                                                        </div>
                                                        <div class="col-xl-6">
                                                            <!--begin::Input-->
                                                            <div class="form-group">
                                                                <label>E-mail</label>
                                                                <input type="email" wire:model="email" class="form-control form-control-solid form-control-lg" name="email" placeholder="Email" />
                                                                <span class="form-text text-muted">Please enter your Email.</span>
                                                                @error('email') <span class="error">{{ $message }}</span> @enderror
                                                            </div>
                                                            <!--end::Input-->
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xl-6">
                                                            <!--begin::Input-->
                                                            <div class="form-group">
                                                                <label>Phone Number</label>
                                                                <input type="number" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength = "10" wire:model="phonenumber" class="form-control form-control-solid form-control-lg" name="phonenumber" placeholder="Phone Number" />
                                                                <span class="form-text text-muted">Please enter your Phone Number</span>
                                                                @error('phonenumber') <span class="error">{{ $message }}</span> @enderror
                                                            </div>
                                                            <!--end::Input-->
                                                        </div>
                                                        <div class="col-xl-6">
                                                            <!--begin::Input-->
                                                            <div class="form-group">
                                                                <label>National ID No</label>
                                                                <input type="number" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength = "16" wire:model="nationalDNo" class="form-control form-control-solid form-control-lg" name="nationald" placeholder="National ID No" />
                                                                <span class="form-text text-muted">Please enter your National ID No.</span>
                                                                @error('nationalDNo') <span class="error">{{ $message }}</span> @enderror
                                                            </div>
                                                            <!--end::Input-->
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xl-6">
                                                            <!--begin::Input-->
                                                            <div class="form-group">
                                                                <label>Title</label>
                                                                <input type="text" wire:model="title" class="form-control form-control-solid form-control-lg" name="Title" placeholder="Title" />
                                                                <span class="form-text text-muted">Please enter your Position.</span>
                                                                @error('title') <span class="error">{{ $message }}</span> @enderror
                                                            </div>
                                                            <!--end::Input-->
                                                        </div>
                                                        <div class="col-xl-6">
                                                            <!--begin::Input-->
                                                            <div class="form-group">
                                                                <label>Gender</label>
                                                                <select name="gender" wire:model="gender" class="form-control form-control-solid form-control-lg">
                                                                    <option selected="selected">Select Gender</option>
                                                                    <option value="Male">Male</option>
                                                                    <option value="Female">Female</option>
                                                                </select>
                                                                @error('gender') <span class="error">{{ $message }}</span> @enderror
                                                            </div>
                                                            <!--end::Input-->
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xl-6">
                                                            <!--begin::Input-->
                                                            <div class="form-group">
                                                                <label>Province/Intara</label>
                                                                <select name="province" wire:model="province" class="form-control form-control-solid form-control-lg">
                                                                    <option selected="selected">Select Province/Intara</option>
                                                                    @foreach($provinces as $province_)
                                                                        <option value={{ $province_->id }}>{{ $province_->name }}</option>
                                                                    @endforeach
                                                                </select>
                                                                @error('province') <span class="error">{{ $message }}</span> @enderror
                                                            </div>
                                                            <!--end::Input-->
                                                        </div>
                                                        @if(count($districts) > 0)
                                                            <div class="col-xl-6">
                                                                <!--begin::Input-->
                                                                <label>District/Akarere</label>
                                                                <select name="district" wire:model="district" class="form-control form-control-solid form-control-lg">
                                                                    <option selected="selected">District/Akarere</option>
                                                                    @foreach($districts as $district_)
                                                                        <option value={{ $district_->id }}>{{ $district_->name }}</option>
                                                                    @endforeach
                                                                </select>
                                                                @error('district') <span class="error">{{ $message }}</span> @enderror
                                                            <!--end::Input-->
                                                            </div>
                                                        @endif
                                                    </div>
                                                    <div class="row">
                                                        @if(count($sectors) > 0)
                                                            <div class="col-xl-4">
                                                                <!--begin::Input-->
                                                                <div class="form-group">
                                                                    <label>Sector/Umurenge</label>
                                                                    <select name="sector" wire:model="sector" class="form-control form-control-solid form-control-lg">
                                                                        <option selected="selected">Sector/Umurenge</option>
                                                                        @foreach($sectors as $sector_)
                                                                            <option value={{ $sector_->id }}>{{ $sector_->name }}</option>
                                                                        @endforeach
                                                                    </select>
                                                                    @error('sector') <span class="error">{{ $message }}</span> @enderror
                                                                </div>
                                                                <!--end::Input-->
                                                            </div>
                                                        @endif
                                                        @if(count($cells) > 0)
                                                            <div class="col-xl-4">
                                                                <!--begin::Input-->
                                                                <label>Cell/Akagari</label>
                                                                <select name="cell" wire:model="cell" class="form-control form-control-solid form-control-lg">
                                                                    <option selected="selected">Cell/Akagari</option>
                                                                    @foreach($cells as $cells_)
                                                                        <option value={{ $cells_->id }}>{{ $cells_->name }}</option>
                                                                    @endforeach
                                                                </select>
                                                                @error('cell') <span class="error">{{ $message }}</span> @enderror
                                                            <!--end::Input-->
                                                            </div>
                                                        @endif
                                                        @if(count($villages) > 0)
                                                            <div class="col-xl-4">
                                                                <!--begin::Input-->
                                                                <label>Village/Umudugudu</label>
                                                                <select name="village" wire:model="village" class="form-control form-control-solid form-control-lg">
                                                                    <option selected="selected">Village/Umudugudu</option>
                                                                    @foreach($villages as $village_)
                                                                        <option value={{ $village_->id }}>{{ $village_->name }}</option>
                                                                    @endforeach
                                                                </select>
                                                                @error('village') <span class="error">{{ $message }}</span> @enderror
                                                            <!--end::Input-->
                                                            </div>
                                                        @endif
                                                    </div>
{{--                                                    <div class="row">--}}
{{--                                                        <div class="col-xl-6">--}}
{{--                                                            <!--begin::Input-->--}}
{{--                                                            <div class="form-group">--}}
{{--                                                                <label>Password</label>--}}
{{--                                                                <input type="password" wire:model="password" class="form-control form-control-solid form-control-lg" placeholder="*******" />--}}
{{--                                                                <span class="form-text text-muted">Please enter your password.</span>--}}
{{--                                                                @error('password') <span class="error">{{ $message }}</span> @enderror--}}
{{--                                                            </div>--}}
{{--                                                            <!--end::Input-->--}}
{{--                                                        </div>--}}
{{--                                                        <div class="col-xl-6">--}}
{{--                                                            <!--begin::Input-->--}}
{{--                                                            <div class="form-group">--}}
{{--                                                                <label>Confirm Password</label>--}}
{{--                                                                <input type="password" class="form-control form-control-solid form-control-lg" name="confirmpassword" placeholder="*******" />--}}
{{--                                                                <span class="form-text text-muted">Confirm password.</span>--}}
{{--                                                            </div>--}}
{{--                                                            <!--end::Input-->--}}
{{--                                                        </div>--}}
{{--                                                    </div>--}}
                                                </div>
                                                <div class="modal-footer">
                                                    <div wire:loading wire:target="submit">
                                                        <img style="width: 100px;" src="https://media.tenor.com/images/d5015577b1133a47299b149b6fab1aaa/tenor.gif">
                                                    </div>
                                                    <button wire:loading.remove wire:target="submit" type="submit" class="btn btn-primary">Create account</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!--end::Card-->
                        </div>
                        <!--end::Container-->
                    </div>
                    <!--end::Entry-->
                </div>
                <!--end::Content-->
                <!--begin::Footer-->
                <livewire:backend.footer />
                <!--end::Footer-->
            </div>
            <!--end::Wrapper-->
        </div>
        <!--end::Page-->
    </div>
    <!--end::Main-->
</div>
@push('scripts')
    <script type="text/javascript">
        window.livewire.on('userUpdate', () => {
            $('#newUserStaff').modal('hide');
        });
    </script>
@endpush
