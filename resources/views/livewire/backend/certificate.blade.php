<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>Card</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
    <style>
        @page {
            margin: 0px;
        }
        body {
            margin: 0px;
        }
        .card {
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
            max-width: 100%; /* margin: auto; */
            text-align: center;
            position: relative; /* top: 50%; */ /* left: 50%; */
            -ms-transform: translate(-50%, -50%); /* transform: translate(-50%, -50%); */
            font-family: arial;
            width: 100%;
        }
        .title {
            color: #194d84;
            font-size: 45px;
            font-weight: bold;
        }
        button {
            border: none;
            outline: 0;
            display: inline-block;
            padding: 8px;
            color: white;
            background-color: #184d84;
            text-align: center;
            cursor: pointer;
            width: 100%;
        }
    </style>
</head>
<body style="background: white; width: 100%; margin: 0;">
    <p align="center">
        <img
            width="351"
            height="61"
            src="{{ asset('backend/assets/media/logos/logo.png') }}"
            alt="Logo"
        />
    </p>
    <p align="center">
        <strong>Membership Certificate</strong>
    </p>
    <p>
        This to certify that <strong>Names </strong> is a Rwanda Youth in
        Agribusiness Forum (RYAF) Member since    <strong>year of Registration</strong>.
    </p>
    <p>
        Date: Signed By :
    </p>
</body>
</html>
