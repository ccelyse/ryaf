@push('styles')
    <style>
        @media (min-width: 992px){
            .login.login-1 .login-content {
                width: 100%;
                max-width: 100% !important;
            }
        }
        ul li{
            margin: 15px;
            color: #000;
        }
        .error{
            color: red !important;
        }
    </style>
@endpush
<div>
    <!--begin::Main-->
    <div class="d-flex flex-column flex-root">
        <!--begin::Login-->
        <div class="login login-1 login-signin-on d-flex flex-column flex-lg-row flex-column-fluid bg-white" id="kt_login">
            <!--begin::Aside-->
            <div class="login-aside d-flex flex-column flex-row-auto" style="background-color: #F2C98A;">
                <!--begin::Aside Top-->
                <div class="d-flex flex-column-auto flex-column pt-15">
                    <!--begin::Aside header-->
                    <a href="{{'/'}}" class="text-center mb-10">
                        <img src="{{ asset('backend/assets/media/logos/logo.png') }}" class="max-h-70px" alt="" />
                    </a>
                    <!--end::Aside header-->
                    <!--begin::Aside title-->
                    <h3 class="font-weight-bolder text-center font-size-h4 font-size-h1-lg" style="color: #619c41;">The Rwanda Youth in Agribusiness Forum
                        <br />(RYAF)</h3>
                    <p style="color: #000; padding: 20px; font-size: 16px;">Being a RYAF member  will Belonging to a highly organized professional community of
                        like-minded young entrepreneurs which is well-connected and coordinated from grassroots to national level.</p>
                    <h3 class="font-weight-bolder text-center font-size-h4 font-size-h1-lg" style="color: #619c41;">What are Member benefits to expect?</h3>
                    <ul>
                        <li>Belonging to a highly organized professional community of like-minded young agripreneurs which is well-connected and coordinated from grassroots to national level.</li>
                        <li>Opportunity to promote yourself and your achievements through RYAF networks</li>
                        <li>Easy access to global opportunities designed for youth in agribusiness.</li>
                        <li>Business Development Support</li>
                        <li>Mentoring and Coaching</li>
                        <li>Access to RYAF Capacity Building Programs</li>
                        <li>Access to markets for members through partner buyers or RYAF shops.</li>
                        <li>Face-to-face and online events</li>
                        <li>Latest industry news and thought leadership articles</li>
                        <li>Access to prestigious, exclusively negotiated benefits with hand-picked partners.</li>
                    </ul>
                    <!--end::Aside title-->
                </div>
                <!--end::Aside Top-->
                <!--begin::Aside Bottom-->
            {{--                <div class="aside-img d-flex flex-row-fluid bgi-no-repeat bgi-position-y-bottom bgi-position-x-center" style="background-image: url(https://preview.keenthemes.com/metronic/theme/html/demo2/dist/assets/media/svg/illustrations/login-visual-1.svg)"></div>--}}
            <!--end::Aside Bottom-->
            </div>
            <!--begin::Aside-->
            <!--begin::Content-->
            <div class="login-content flex-row-fluid d-flex flex-column justify-content-center position-relative overflow-hidden p-7 mx-auto">
                <!--begin::Content body-->
                <div class="d-flex flex-column-fluid">
                    <!--begin::Container-->
                    <div class="container">
                        <div class="card card-custom">
                            <div class="card-body p-0">
                                <!--begin::Wizard-->
                                <div class="wizard wizard-1" id="kt_wizard" data-wizard-state="step-first" data-wizard-clickable="false">
                                    <!--begin::Wizard Nav-->
                                    <div class="wizard-nav border-bottom">
                                        <div class="wizard-steps p-8 p-lg-10">
                                            <!--begin::Wizard Step 1 Nav-->
                                            <div class="wizard-step" data-wizard-type="step" data-wizard-state="current">
                                                <div class="wizard-label">
                                                    <i class="wizard-icon flaticon-bus-stop"></i>
                                                    <h3 class="wizard-title">Check with Membership code to download certificate</h3>
                                                </div>
                                                <span class="svg-icon svg-icon-xl wizard-arrow">
															<!--begin::Svg Icon | path:/metronic/theme/html/demo2/dist/assets/media/svg/icons/Navigation/Arrow-right.svg-->
															<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
																<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																	<polygon points="0 0 24 0 24 24 0 24" />
																	<rect fill="#000000" opacity="0.3" transform="translate(12.000000, 12.000000) rotate(-90.000000) translate(-12.000000, -12.000000)" x="11" y="5" width="2" height="14" rx="1" />
																	<path d="M9.70710318,15.7071045 C9.31657888,16.0976288 8.68341391,16.0976288 8.29288961,15.7071045 C7.90236532,15.3165802 7.90236532,14.6834152 8.29288961,14.2928909 L14.2928896,8.29289093 C14.6714686,7.914312 15.281055,7.90106637 15.675721,8.26284357 L21.675721,13.7628436 C22.08284,14.136036 22.1103429,14.7686034 21.7371505,15.1757223 C21.3639581,15.5828413 20.7313908,15.6103443 20.3242718,15.2371519 L15.0300721,10.3841355 L9.70710318,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.999999, 11.999997) scale(1, -1) rotate(90.000000) translate(-14.999999, -11.999997)" />
																</g>
															</svg>
                                                    <!--end::Svg Icon-->
														</span>
                                            </div>
                                            <!--end::Wizard Step 1 Nav-->
                                        </div>
                                    </div>
                                    <!--end::Wizard Nav-->
                                    <!--begin::Wizard Body-->
                                    <div class="row justify-content-center my-10 px-8 my-lg-15 px-lg-10">
                                        <div class="col-xl-12 col-xxl-7">
                                            @if (session('success'))
                                                <div class="alert alert-primary mb-4" role="alert" style="margin: 0 auto;">
                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
                                                    </button>
                                                    <strong>Success!</strong> {{ session('success') }}
                                                    @if(!empty($User_check))
                                                        <a href="{{'DownloadCertificate'}}/{{$user_id}}" class="btn btn-success font-weight-bolder text-uppercase px-9 py-4"><strong>Download certificate!</strong></a>
                                                    @endif
                                                </div>
                                            @endif
                                        <!--begin::Wizard Form-->
                                            <form class="form" id="kt_form" wire:submit.prevent="submit">
                                                <!--begin::Wizard Step 1-->
                                                <div class="pb-5" data-wizard-type="step-content" data-wizard-state="current">
                                                    <h3 class="mb-10 font-weight-bold text-dark"></h3>
                                                    <!--begin::Input-->
                                                    <!-- <div class="form-group">
                                                        <label>Address Line 1</label>
                                                        <input type="text" class="form-control form-control-solid form-control-lg" name="address1" placeholder="Address Line 1" value="Address Line 1" />
                                                        <span class="form-text text-muted">Please enter your Address.</span>
                                                    </div> -->
                                                    <!--end::Input-->
                                                    <!--begin::Input-->
                                                    <div class="row">
                                                        <div class="col-xl-6">
                                                            <!--begin::Input-->
                                                            <div class="form-group">
                                                                <label>Membership Code</label>
                                                                <input type="text" wire:model="membership_code" class="form-control form-control-solid form-control-lg" name="membership_code" placeholder="Code" />
                                                                <span class="form-text text-muted">Please enter your Membership Code.</span>
                                                                @error('names') <span class="error">{{ $message }}</span> @enderror
                                                            </div>
                                                            <!--end::Input-->
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--end::Wizard Step 1-->
                                                <!--begin::Wizard Actions-->
                                                <div class="d-flex justify-content-between border-top mt-5 pt-10">
                                                    <div class="mr-2">
                                                        <a href="{{'/'}}" class="btn btn-light-primary font-weight-bolder text-uppercase px-9 py-4">Go back Home</a>
                                                    </div>
                                                    <div>
                                                        <button type="submit" class="btn btn-success font-weight-bolder text-uppercase px-9 py-4">Check</button>
                                                    </div>
                                                </div>
                                            <!--end::Wizard Actions-->
                                            </form>
                                            <!--end::Wizard Form-->
                                        </div>
                                    </div>
                                    <!--end::Wizard Body-->
                                </div>
                                <!--end::Wizard-->
                            </div>
                            <!--end::Wizard-->
                        </div>
                    </div>
                    <!--end::Container-->
                </div>
                <!--end::Content body-->
                <!--begin::Content footer-->
                <div class="d-flex justify-content-lg-start justify-content-center align-items-end py-7 py-lg-0">
                    <div class="text-dark-50 font-size-lg font-weight-bolder mr-10">
                        <span class="mr-1">2021©</span>
                        <a href="#" target="_blank" class="text-dark-75 text-hover-primary">RYAF</a>
                    </div>
                    <a href="#" class="text-primary font-weight-bolder font-size-lg">Terms</a>
                    <a href="#" class="text-primary ml-5 font-weight-bolder font-size-lg">Contact Us</a>
                </div>
                <!--end::Content footer-->
            </div>
            <!--end::Content-->
        </div>
        <!--end::Login-->
    </div>

</div>
