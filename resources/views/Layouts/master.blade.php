<!DOCTYPE html>
<html lang="en">
<head>

    <title>@yield('title')</title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="shortcut icon" href="{{ asset('assets/img/favicon.png')}}">

    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@400;500;700&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('frontend/assets/vendor/font-awesome/css/fontawesome-all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('frontend/assets/vendor/flaticon/font/flaticon.css') }}">
    <link rel="stylesheet" href="{{ asset('frontend/assets/vendor/animate.css/animate.css') }}">
    <link rel="stylesheet" href="{{ asset('frontend/assets/vendor/bootstrap-select/dist/css/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('frontend/assets/vendor/slick-carousel/slick/slick.css') }}" />
    <link rel="stylesheet" href="{{ asset('frontend/assets/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css') }}">
    <link rel="stylesheet" href="{{ asset('frontend/assets/css/theme.css') }}">

{{--    <link rel="stylesheet" href="{{ asset('../css/simditor.css') }}">--}}
    <style>
        .invalid-feedback {
            display: block !important;
            width: 100%;
            margin-top: .25rem;
            font-size: 80%;
            color: #f75454;
        }
        .bg-carolina-light {
            background-color: #F0C579!important;
        }
        .header_custom{
            background: url('https://www.wattpad.com/writers/static/highlight-1-cb98e63137d0c228438500d4bc13bd85.svg');
            /*font-size: 18px;*/
            font-weight: 700;
            text-align: center;
            /*color: #222;*/
            width: 229px;
            height: 45px;
            line-height: 45px;
        }
        .panel {
            border: 2px solid transparent;
            position: relative;
            border-radius: 8px;
        }
        @media (min-width: 0px){
            .space-bottom-xs-md {
                margin-bottom: 16px !important;
            }
        }

        .hover-grow {
            -webkit-transition: all 0.2s ease;
            -moz-transition: all 0.2s ease;
            -ms-transition: all 0.2s ease;
            -o-transition: all 0.2s ease;
            transition: all 0.2s ease;
        }
        .box-shadow-xl, .dropdown .dropdown-list, .video-container {
            /*box-shadow: 0 1px 5px 0 rgb(0 0 0 / 0%), 0 8px 16px 0 rgb(0 0 0 / 5%);*/
            box-shadow: 0 1px 5px 0 rgba(0,0,0,0.1), 0 8px 16px 0 rgba(0,0,0,0.08);
        }
        .hover-grow:hover {
            transform: scale3d(1.02, 1.02, 1);
        }
        .hover-grow {
            -webkit-transition: all 0.2s ease;
            -moz-transition: all 0.2s ease;
            -ms-transition: all 0.2s ease;
            -o-transition: all 0.2s ease;
            transition: all 0.2s ease;
        }
        .panel.current {
            border: 2px solid #edc001;
        }
        .panel.panel-thin>.panel-body {
            padding: 16px;
        }
        @media (min-width: 900px){
            .panel .panel-body {
                padding: 32px;
            }
        }
        .panel .panel-body {
            padding: 16px;
            box-sizing: border-box;
        }
        .gutter-md {
            margin: -16px 0 0 -16px;
        }
        .align-middle {
            align-items: center;
        }
        .grid {
            display: flex;
            flex-flow: row wrap;
            box-sizing: border-box;
        }
        .gutter-md>:nth-child(n) {
            padding-top: 16px;
            padding-left: 16px;
        }
        .cell-shrink {
            flex: 0 1 auto;
        }
        .cell {
            flex: 1;
            min-width: 0;
        }
        .border-right {
            border-right: 2px solid #e9ecef;
            height: 100%;
        }
        .mimic-h4 {
            font-size: 18px;
            line-height: 24px;
        }
        .x-small {
            font-size: 14px;
            line-height: 18px;
        }
        @media (min-width: 0px){
            .space-top-xs-xs {
                margin-top: 4px !important;
            }
        }
        span.btn-modal-trigger {
            font-weight: bold;
        }
        .small {
            font-size: 16px;
            line-height: 24px;
        }
        .text-green, section a {
            color: #91B827 !important;
            transition: color 0.2s ease;
        }
        .btn-modal-trigger {
            cursor: pointer;
        }
        .panel-body a {
            color: #91B827;
        }
        a.text-grey {
            color: #6c757d !important;
        }
        .img-avatar.avatar-2lg {
            width: 64px;
            height: 64px;
            border-radius: 50%;
        }
        .recommendations-container .recommendation-gradient {
            position: absolute;
            height: 160px;
            width: 100%;
            margin: -8px;
            bottom: 0px;
            z-index: 1;
            background: rgba(255,255,255,0);
            background: -moz-linear-gradient(top, rgba(255,255,255,0) 0%, #fff 100%);
            background: -webkit-gradient(linear, top, bottom, color-stop(0%, #fff), color-stop(100%, #fff));
            background: -webkit-linear-gradient(top, rgba(255,255,255,0) 0%, #fff 100%);
            background: -o-linear-gradient(top, rgba(255,255,255,0) 0%, #fff 100%);
            background: -ms-linear-gradient(top, rgba(255,255,255,0) 0%, #fff 100%);
            background: linear-gradient(to bottom, rgba(255,255,255,0) 0%, #fff 100%);
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#000000',GradientType=0 );
        }
        .recommendations-container .recommendation-button {
            position: absolute;
            padding: 8px 16px;
            border-radius: 20px;
            max-width: 120px;
            bottom: 60px;
            left: 0;
            right: 0;
            margin-left: auto;
            margin-right: auto;
            text-align: center;
            background: #2B353A;
            box-shadow: 0px 2px 40px rgba(0,0,0,0.15);
            color: #fff !important;
            text-transform: uppercase;
            font-weight: bold;
            z-index: 2;
            cursor: pointer;
            font-size: 11px;
        }

        .current {
            background: #FDF5D9;
            border: 2px solid #EDC001;
            cursor: pointer;
            border-radius: 20px;
        }
        .simditor {
            position: relative;
            border: 1px solid #c9d8db;
            width: 100% !important;
        }
        .simditor-placeholder{
            top: 30px !important;
        }
        .text-green, section a {
            color: #0c1e45 !important;
            transition: color 0.2s ease;
        }
        .label-blue{
            /*background-color: #0C1E45;*/
            /*color: #FFFFFF;*/
            border-width: 1px;
            border-style: solid;
            /*border-color: #0C1E45;*/
            display: inline-block;
            font-weight: 700;
            margin: 0 9px 0 0;
            border-radius: 3px;
            padding: 7px;

            margin-bottom: 15px;
            background: #eadcd2;
            border: #eadcd2;
            color: #0c1e45
        }
        .add_field_button{
            border-radius: 20px;
            margin-bottom: 10px;
        }
        .remove_field{
            border-radius: 0px;
            margin-bottom: 10px;
            margin-top: 10px;
        }
        /* The message box is shown when the user clicks on the password field */
        #message {
            display:none;
            background: #f1f1f1;
            color: #000;
            position: relative;
            padding: 20px;
            margin-top: 10px;
        }

        #message p {
            padding: 10px 35px;
            font-size: 13px;
        }

        /* Add a green text color and a checkmark when the requirements are right */
        .valid {
            color: green;
        }

        .valid:before {
            position: relative;
            left: -35px;
            content: "✔";
        }

        /* Add a red text color and an "x" when the requirements are wrong */
        .invalid {
            color: red;
        }

        .invalid:before {
            position: relative;
            left: -35px;
            content: "✖";
        }
        #primary, #secondary {
            position: relative;
            padding-right: 15px;
            padding-left: 15px;
        }

        .masthead{
            background: #fff;
        }
        .link-black-100 {
            color: #007b9f !important;
            font-size: 16px !important;
            font-weight: bold !important;
        }
        .dropdown-menu {
            background-color: #fff !important;
        }
        .dropdown-item:hover, .dropdown-item:focus {
            background-color: #f0c579 !important;
        }
        .hidewhenloggedin{
            display: none !important;
        }
        .link-black-124 {
            color: #161619 !important;
        }
        .ml-4, .mx-4 {
            margin-left: 1rem!important;
        }
    </style>
    @livewireStyles
    @stack('styles')
</head>
<body>
<livewire:frontend.header />
<aside id="sidebarContent" class="u-sidebar u-sidebar__lg" aria-labelledby="sidebarNavToggler">
    <div class="u-sidebar__scroller">
        <div class="u-sidebar__container">
            <div class="u-header-sidebar__footer-offset">
                <div class="d-flex align-items-center position-absolute top-0 right-0 z-index-2 mt-5 mr-md-6 mr-4">
                    <button type="button" class="close ml-auto" aria-controls="sidebarContent" aria-haspopup="true" aria-expanded="false" data-unfold-event="click" data-unfold-hide-on-scroll="false" data-unfold-target="#sidebarContent" data-unfold-type="css-animation" data-unfold-animation-in="fadeInRight" data-unfold-animation-out="fadeOutRight" data-unfold-duration="500">
                        <span aria-hidden="true">Close <i class="fas fa-times ml-2"></i></span>
                    </button>
                </div>
                <div class="js-scrollbar u-sidebar__body">
                    <div class="u-sidebar__content u-header-sidebar__content">

                        <div id="login_" data-target-group="idForm">
                            <header class="border-bottom px-4 px-md-6 py-4">
                                <h2 class="font-size-3 mb-0 d-flex align-items-center">
                                    <i class="flaticon-user mr-3 font-size-5"></i>Account</h2>
                            </header>
                            <form class="mt-4 form-text" action="{{ route('login') }}" method="POST">
                                @csrf
                                <div class="p-4 p-md-6">
                                    <div class="form-group mb-4">
                                        <div class="js-form-message js-focus-state">
                                            <label id="signinEmailLabel" class="form-label" for="signinEmail">Email *</label>
                                            <input type="email" class="form-control rounded-0 height-4 px-4" name="email" id="signinEmail" placeholder="creativelayers088@gmail.com" required>
                                        </div>
                                        @error('email')
                                        <span class="invalid-feedback is-invalid" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="form-group mb-4">
                                        <div class="js-form-message js-focus-state">
                                            <label id="signinPasswordLabel" class="form-label" for="signinPassword">Password *</label>
                                            <input type="password" class="form-control rounded-0 height-4 px-4" name="password" id="signinPassword" placeholder="" required>
                                        </div>
                                        @error('password')
                                        <span class="invalid-feedback is-invalid" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="d-flex justify-content-between mb-5 align-items-center">
                                        <div class="js-form-message">
                                            <div class="custom-control custom-checkbox d-flex align-items-center text-muted">
                                                <input type="checkbox" class="custom-control-input" id="termsCheckbox" name="termsCheckbox">
                                                <label class="custom-control-label" for="termsCheckbox">
                                                    <span class="font-size-2 text-secondary-gray-700">
                                                    Remember me
                                                    </span>
                                                </label>
                                            </div>
                                        </div>
                                        <a class="js-animation-link text-dark font-size-2 t-d-u link-muted font-weight-medium" href="javascript:;" data-target="#forgotPassword" data-link-group="idForm" data-animation-in="fadeIn">Forgot Password?</a>
                                    </div>
                                    <div class="mb-4d75">
                                        <button type="submit" class="btn btn-block py-3 rounded-0 btn-dark">Sign In</button>
                                    </div>
                                    <p>Don't have any account? Sign up as a <a href="{{'/RegisterTrainee'}}">Trainees</a> or <a href="{{'/RegisterTrainer'}}">Trainer</a> </p>

                                </div>
                            </form>
                        </div>


                        <div id="forgotPassword" style="display: none; opacity: 0;" data-target-group="idForm">

                            <header class="border-bottom px-4 px-md-6 py-4">
                                <h2 class="font-size-3 mb-0 d-flex align-items-center"><i class="flaticon-question mr-3 font-size-5"></i>Forgot Password?</h2>
                            </header>

                            <div class="p-4 p-md-6">

                                <div class="form-group mb-4">
                                    <div class="js-form-message js-focus-state">
                                        <label id="signinEmailLabel3" class="form-label" for="signinEmail3">Email *</label>
                                        <input type="email" class="form-control rounded-0 height-4 px-4" name="email" id="signinEmail3" placeholder="creativelayers088@gmail.com" aria-label="creativelayers088@gmail.com" aria-describedby="signinEmailLabel3" required>
                                    </div>
                                </div>

                                <div class="mb-3">
                                    <button type="submit" class="btn btn-block py-3 rounded-0 btn-dark">Recover Password</button>
                                </div>
                                <div class="text-center mb-4">
                                    <span class="small text-muted">Remember your password?</span>
                                    <a class="js-animation-link small" href="javascript:;" data-target="#login" data-link-group="idForm" data-animation-in="fadeIn">Login
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</aside>
<aside id="sidebarContent1" class="u-sidebar u-sidebar__xl" aria-labelledby="sidebarNavToggler1">
    <div class="u-sidebar__scroller js-scrollbar">
        <div class="u-sidebar__container">
            <div class="u-header-sidebar__footer-offset">

                <div class="d-flex align-items-center position-absolute top-0 right-0 z-index-2 mt-5 mr-md-6 mr-4">
                    <button type="button" class="close ml-auto" aria-controls="sidebarContent1" aria-haspopup="true" aria-expanded="false" data-unfold-event="click" data-unfold-hide-on-scroll="false" data-unfold-target="#sidebarContent1" data-unfold-type="css-animation" data-unfold-animation-in="fadeInRight" data-unfold-animation-out="fadeOutRight" data-unfold-duration="500">
                        <span aria-hidden="true">Close <i class="fas fa-times ml-2"></i></span>
                    </button>
                </div>

                <div class="u-sidebar__body">
                    <div class="u-sidebar__content u-header-sidebar__content">

                        <header class="border-bottom px-4 px-md-6 py-4">
                            <h2 class="font-size-3 mb-0 d-flex align-items-center"><i class="flaticon-icon-126515 mr-3 font-size-5"></i>Your shopping bag (3)</h2>
                        </header>

                        <div class="px-4 py-5 px-md-6 border-bottom">
                            <div class="media">
                                <a href="#" class="d-block"><img src="https://placehold.it/100x153" class="img-fluid" alt="image-description"></a>
                                <div class="media-body ml-4d875">
                                    <div class="text-primary text-uppercase font-size-1 mb-1 text-truncate"><a href="#">Hard Cover</a></div>
                                    <h2 class="woocommerce-loop-product__title h6 text-lh-md mb-1 text-height-2 crop-text-2">
                                        <a href="#" class="text-dark">The Ride of a Lifetime: Lessons Learned from 15 Years as CEO</a>
                                    </h2>
                                    <div class="font-size-2 mb-1 text-truncate"><a href="#" class="text-gray-700">Robert Iger</a></div>
                                    <div class="price d-flex align-items-center font-weight-medium font-size-3">
                                        <span class="woocommerce-Price-amount amount">1 x <span class="woocommerce-Price-currencySymbol">$</span>125.30</span>
                                    </div>
                                </div>
                                <div class="mt-3 ml-3">
                                    <a href="#" class="text-dark"><i class="fas fa-times"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="px-4 py-5 px-md-6 border-bottom">
                            <div class="media">
                                <a href="#" class="d-block"><img src="https://placehold.it/100x153" class="img-fluid" alt="image-description"></a>
                                <div class="media-body ml-4d875">
                                    <div class="text-primary text-uppercase font-size-1 mb-1 text-truncate"><a href="#">Hard Cover</a></div>
                                    <h2 class="woocommerce-loop-product__title h6 text-lh-md mb-1 text-height-2 crop-text-2">
                                        <a href="#" class="text-dark">The Rural Diaries: Love, Livestock, and Big Life Lessons Down</a>
                                    </h2>
                                    <div class="font-size-2 mb-1 text-truncate"><a href="#" class="text-gray-700">Hillary Burton</a></div>
                                    <div class="price d-flex align-items-center font-weight-medium font-size-3">
                                        <span class="woocommerce-Price-amount amount">2 x <span class="woocommerce-Price-currencySymbol">$</span>200</span>
                                    </div>
                                </div>
                                <div class="mt-3 ml-3">
                                    <a href="#" class="text-dark"><i class="fas fa-times"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="px-4 py-5 px-md-6 border-bottom">
                            <div class="media">
                                <a href="#" class="d-block"><img src="https://placehold.it/100x153" class="img-fluid" alt="image-description"></a>
                                <div class="media-body ml-4d875">
                                    <div class="text-primary text-uppercase font-size-1 mb-1 text-truncate"><a href="#">Paperback</a></div>
                                    <h2 class="woocommerce-loop-product__title h6 text-lh-md mb-1 text-height-2 crop-text-2">
                                        <a href="#" class="text-dark">Russians Among Us: Sleeper Cells, Ghost Stories, and the Hunt.</a>
                                    </h2>
                                    <div class="font-size-2 mb-1 text-truncate"><a href="#" class="text-gray-700">Gordon Corera</a></div>
                                    <div class="price d-flex align-items-center font-weight-medium font-size-3">
                                        <span class="woocommerce-Price-amount amount">6 x <span class="woocommerce-Price-currencySymbol">$</span>100</span>
                                    </div>
                                </div>
                                <div class="mt-3 ml-3">
                                    <a href="#" class="text-dark"><i class="fas fa-times"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="px-4 py-5 px-md-6 d-flex justify-content-between align-items-center font-size-3">
                            <h4 class="mb-0 font-size-3">Subtotal:</h4>
                            <div class="font-weight-medium">$750.00</div>
                        </div>
                        <div class="px-4 mb-8 px-md-6">
                            <button type="submit" class="btn btn-block py-4 rounded-0 btn-outline-dark mb-4">View Cart</button>
                            <button type="submit" class="btn btn-block py-4 rounded-0 btn-dark">Checkout</button>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</aside>
{{--<aside id="sidebarContent2" class="u-sidebar u-sidebar__md u-sidebar--left" aria-labelledby="sidebarNavToggler2">--}}
{{--    <div class="u-sidebar__scroller js-scrollbar">--}}
{{--        <div class="u-sidebar__container">--}}
{{--            <div class="u-header-sidebar__footer-offset">--}}

{{--                <div class="u-sidebar__body">--}}
{{--                    <div class="u-sidebar__content u-header-sidebar__content">--}}

{{--                        <header class="border-bottom px-4 px-md-5 py-4 d-flex align-items-center justify-content-between">--}}
{{--                            <h2 class="font-size-3 mb-0">MIS</h2>--}}

{{--                            <div class="d-flex align-items-center">--}}
{{--                                <button type="button" class="close ml-auto" aria-controls="sidebarContent2" aria-haspopup="true" aria-expanded="false" data-unfold-event="click" data-unfold-hide-on-scroll="false" data-unfold-target="#sidebarContent2" data-unfold-type="css-animation" data-unfold-animation-in="fadeInLeft" data-unfold-animation-out="fadeOutLeft" data-unfold-duration="500">--}}
{{--                                    <span aria-hidden="true"><i class="fas fa-times ml-2"></i></span>--}}
{{--                                </button>--}}
{{--                            </div>--}}

{{--                        </header>--}}

{{--                        <div class="px-4 px-md-5 pt-5 pb-4 border-bottom">--}}
{{--                            <h2 class="font-size-3 mb-3">HELP & SETTINGS </h2>--}}
{{--                            <ul class="list-group list-group-flush list-group-borderless">--}}
{{--                                <li class="list-group-item px-0 py-2 border-0"><a href="#" class="h-primary">Your Account</a></li>--}}
{{--                                <li class="list-group-item px-0 py-2 border-0"><a href="#" class="h-primary">Help</a></li>--}}
{{--                                <li class="list-group-item px-0 py-2 border-0"><a href="#"  id="sidebarNavToggler" href="javascript:;" role="button" class="h-primary" aria-controls="sidebarContent" aria-haspopup="true" aria-expanded="false" data-unfold-event="click" data-unfold-hide-on-scroll="false" data-unfold-target="#sidebarContent" data-unfold-type="css-animation" data-unfold-overlay='{--}}
{{--                                    "className": "u-sidebar-bg-overlay",--}}
{{--                                    "background": "rgba(0, 0, 0, .7)",--}}
{{--                                    "animationSpeed": 500--}}
{{--                                }' data-unfold-animation-in="fadeInRight" data-unfold-animation-out="fadeOutRight" data-unfold-duration="500">Sign In</a></li>--}}
{{--                            </ul>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}

{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</aside>--}}

{{$slot}}

<footer class="bg-gray-404" style="background-color: #fff !important;">
    <div class="border-top space-top-3" style="padding-top: 2rem!important;">
        <div class="border-bottom pb-5">
            <div class="container">

                <div class="row">
                    <div class="col-lg-6 mb-6 mb-lg-0">
                        <div class="pb-1">
                            <a href="{{'/'}}" class="d-inline-block mb-5">
                                <img src="{{ asset('frontend/assets/img/logo.png') }}" style="width: 200px">
                            </a>
                            <p style="font-size: .875rem!important">
                                The Vision of RMI is “To be the leading regional centre of excellence in quality capacity building and skills development in the fields of administration and resources management”.
                            </p>
                            <p style="font-size: .875rem!important">
                                The mission set by legal provisions extensively covers all the responsibilities of RMI. It was broken down into a less sizeable and comprehensive form as follows: “To offer training, consultancy, research and advisory services to the public, private sectors and the civil society in the fields of administration and resources management for national development”.
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-3 mb-6 mb-lg-0">
                        <h4 class="font-size-3 font-weight-medium mb-2 mb-xl-5 pb-xl-1">Online Services</h4>
                        <ul class="list-unstyled mb-0">
                            <li class="pb-2">
                                <a class="widgets-hover transition-3d-hover font-size-2 link-black-124" href="{{'/AboutUs'}}">Smart admin</a>
                            </li>
                            <li class="py-2">
                                <a class="widgets-hover transition-3d-hover font-size-2 link-black-124" href="#">E-Learning system</a>
                            </li>
                            <li class="py-2">
                                <a class="widgets-hover transition-3d-hover font-size-2 link-black-124" href="#">Recruitment Portal</a>
                            </li>
                            <li class="pt-2">
                                <a class="widgets-hover transition-3d-hover font-size-2 link-black-124" href="#">Rwanda Employee Self-Service Portal</a>
                            </li>
                            <li class="pt-2">
                                <a class="widgets-hover transition-3d-hover font-size-2 link-black-124" href="#">E-Procurement</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-lg-3 mb-6 mb-lg-0">
                        <h4 class="font-size-3 font-weight-medium mb-2 mb-xl-5 pb-xl-1">Contact us</h4>
                        <ul class="list-unstyled mb-0">
                            <li class="py-2">
                                <a class="widgets-hover transition-3d-hover font-size-2 link-black-124" href="#">P.O. Box 23 Muhanga, Rwanda.</a>
                            </li>
                            <li class="py-2">
                                <a class="widgets-hover transition-3d-hover font-size-2 link-black-124" href="#">P.O. Box 541 Kigali, Rwanda;</a>
                            </li>
                            <li class="py-2">
                                <a class="widgets-hover transition-3d-hover font-size-2 link-black-124" href="#">KN 87 ST</a>
                            </li>
                            <li class="pt-2">
                                <a class="widgets-hover transition-3d-hover font-size-2 link-black-124" href="#">E-mail: info@rmi.rw</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="space-1">
            <div class="container">
                <div class="d-lg-flex text-center text-lg-left justify-content-between align-items-center">
                    <p class="mb-3 mb-lg-0 font-size-2">©2021 MIS. All rights reserved</p>
                </div>
            </div>
        </div>
    </div>
</footer>
<script src="{{ asset('frontend/assets/vendor/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset('frontend/assets/vendor/jquery-migrate/dist/jquery-migrate.min.js') }}"></script>
<script src="{{ asset('frontend/assets/vendor/popper.js/dist/umd/popper.min.js') }}"></script>
<script src="{{ asset('frontend/assets/vendor/bootstrap/bootstrap.min.js') }}"></script>
<script src="{{ asset('frontend/assets/vendor/bootstrap-select/dist/js/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('frontend/assets/vendor/multilevel-sliding-mobile-menu/dist/jquery.zeynep.js') }}"></script>
<script src="{{ asset('frontend/assets/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js') }}"></script>
<script src="{{ asset('frontend/assets/vendor/slick-carousel/slick/slick.min.js') }}"></script>

<script src="{{ asset('frontend/assets/js/hs.core.js') }}"></script>
<script src="{{ asset('frontend/assets/js/components/hs.unfold.js') }}"></script>
<script src="{{ asset('frontend/assets/js/components/hs.malihu-scrollbar.js') }}"></script>
<script src="{{ asset('frontend/assets/js/components/hs.slick-carousel.js') }}"></script>
<script src="{{ asset('frontend/assets/js/components/hs.show-animation.js') }}"></script>
<script src="{{ asset('frontend/assets/js/components/hs.selectpicker.js') }}"></script>
<script type="text/javascript" src="{{ asset('frontend/js/module.js') }}"></script>
<script type="text/javascript" src="{{ asset('frontend/js/hotkeys.js') }}"></script>
<script type="text/javascript" src="{{ asset('frontend/js/uploader.js') }}"></script>
<script type="text/javascript" src="{{ asset('frontend/js/simditor.js') }}"></script>


<script>
    $(document).on('ready', function () {
        // initialization of unfold component
        $.HSCore.components.HSUnfold.init($('[data-unfold-target]'));

        // initialization of malihu scrollbar
        $.HSCore.components.HSMalihuScrollBar.init($('.js-scrollbar'));

        // initialization of slick carousel
        $.HSCore.components.HSSlickCarousel.init('.js-slick-carousel');

        // initialization of show animations
        $.HSCore.components.HSShowAnimation.init('.js-animation-link');

        // init zeynepjs
        var zeynep = $('.zeynep').zeynep({
            onClosed: function () {
                // enable main wrapper element clicks on any its children element
                $("body main").attr("style", "");

                console.log('the side menu is closed.');
            },
            onOpened: function () {
                // disable main wrapper element clicks on any its children element
                $("body main").attr("style", "pointer-events: none;");

                console.log('the side menu is opened.');
            }
        });

        // handle zeynep overlay click
        $(".zeynep-overlay").click(function () {
            zeynep.close();
        });

        // open side menu if the button is clicked
        $(".cat-menu").click(function () {
            if ($("html").hasClass("zeynep-opened")) {
                zeynep.close();
            } else {
                zeynep.open();
            }
        });
    });
</script>

<script>
    // if($(location).attr('pathname') == '/DesignerRequestQuote'){
    //     $(document).ready(function() {
    //         var max_fields      = 10; //maximum input boxes allowed
    //         var wrapper   		= $(".input_fields_wrap"); //Fields wrapper
    //         var add_button      = $(".add_field_button"); //Add button ID
    //
    //         var x = 1; //initlal text box count
    //         $(add_button).click(function(e){ //on add input button click
    //             e.preventDefault();
    //             if(x < max_fields){ //max input box allowed
    //                 x++; //text box increment
    //                 $(wrapper).append('<div>' +
    //                     '<input type="url" class="input-text form-control" name="mytext[]" id="first_name"  required><a href="#" class="remove_field label-blue"><i class="fas fa-minus"></i></a>' +
    //                     '</div>'); //add input box
    //             }
    //         });
    //
    //         $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
    //             e.preventDefault(); $(this).parent('div').remove(); x--;
    //         })
    //     });
    // }
    // if($(location).attr('pathname') == '/ReaderSignUp'){
    //     var myInput = document.getElementById("password");
    //     var letter = document.getElementById("letter");
    //     var capital = document.getElementById("capital");
    //     var number = document.getElementById("number");
    //     var length = document.getElementById("length");
    //
    //     // When the user clicks on the password field, show the message box
    //     myInput.onfocus = function() {
    //         document.getElementById("message").style.display = "block";
    //     }
    //
    //     // When the user clicks outside of the password field, hide the message box
    //     myInput.onblur = function() {
    //         document.getElementById("message").style.display = "none";
    //     }
    //
    //     // When the user starts to type something inside the password field
    //     myInput.onkeyup = function() {
    //         // Validate lowercase letters
    //         var lowerCaseLetters = /[a-z]/g;
    //         if(myInput.value.match(lowerCaseLetters)) {
    //             letter.classList.remove("invalid");
    //             letter.classList.add("valid");
    //         } else {
    //             letter.classList.remove("valid");
    //             letter.classList.add("invalid");
    //         }
    //
    //         // Validate capital letters
    //         var upperCaseLetters = /[A-Z]/g;
    //         if(myInput.value.match(upperCaseLetters)) {
    //             capital.classList.remove("invalid");
    //             capital.classList.add("valid");
    //         } else {
    //             capital.classList.remove("valid");
    //             capital.classList.add("invalid");
    //         }
    //
    //         // Validate numbers
    //         var numbers = /[0-9]/g;
    //         if(myInput.value.match(numbers)) {
    //             number.classList.remove("invalid");
    //             number.classList.add("valid");
    //         } else {
    //             number.classList.remove("valid");
    //             number.classList.add("invalid");
    //         }
    //
    //         // Validate length
    //         if(myInput.value.length >= 8) {
    //             length.classList.remove("invalid");
    //             length.classList.add("valid");
    //         } else {
    //             length.classList.remove("valid");
    //             length.classList.add("invalid");
    //         }
    //     }
    //     $(function() {
    //         //on keypress
    //         $('#password_confirmation').keyup(function(e) {
    //             //get values
    //             var password = $('#password').val();
    //             var confirm_password = $(this).val();
    //
    //             //check the strings
    //             if (password == confirm_password) {
    //                 //if both are same remove the error and allow to submit
    //                 $('.passwordyes').text('Password are matching');
    //                 $('.passworderror').text('');
    //             } else {
    //                 //if not matching show error and not allow to submit
    //                 $('.passwordyes').text('');
    //                 $('.passworderror').text('Password not matching');
    //             }
    //         });
    //     });
    // }
</script>
@livewireScripts
@stack('scripts')
</body>
</html>
