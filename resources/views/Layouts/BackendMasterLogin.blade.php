<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8" />
    <title>RYAF</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta content="RYAF" name="description" />
    <meta content="" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <!-- App favicon -->
    <link rel="shortcut icon" href="{{ asset('backend/assets/images/favicon.ico')}}">

    <!-- App css -->
    <link href="{{ asset('backend/assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('backend/assets/css/icons.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('backend/assets/css/metisMenu.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('backend/plugins/daterangepicker/daterangepicker.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('backend/assets/css/app.min.css')}}" rel="stylesheet" type="text/css" />
    @stack('styles')
</head>

<body class="account-body accountbg">
<!-- BEGIN LOADER -->
{{--<livewire:backend.header />--}}
{{$slot}}

<!-- BEGIN GLOBAL MANDATORY SCRIPTS -->
<script src="{{ asset('backend/assets/js/jquery.min.js')}}"></script>
<script src="{{ asset('backend/assets/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{ asset('backend/assets/js/metismenu.min.js')}}"></script>
<script src="{{ asset('backend/assets/js/waves.js')}}"></script>
<script src="{{ asset('backend/assets/js/feather.min.js')}}"></script>
<script src="{{ asset('backend/assets/js/simplebar.min.js')}}"></script>
<script src="{{ asset('backend/assets/js/moment.js')}}"></script>
<script src="{{ asset('backend/plugins/daterangepicker/daterangepicker.js')}}"></script>

<script src="{{ asset('backend/plugins/apex-charts/apexcharts.min.js')}}"></script>
<script src="{{ asset('backend/assets/pages/jquery.sales_dashboard.init.js')}}"></script>

<!-- App js -->
@livewireScripts
@stack('scripts')
</body>
</html>
