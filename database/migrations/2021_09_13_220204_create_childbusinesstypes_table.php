<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChildbusinesstypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('childbusinesstypes', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('sub_business_type_id');
            $table->foreign('sub_business_type_id')->references('id')->on('busines_sub_types');
            $table->string('child_sub_name');
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('childbusinesstypes');
    }
}
