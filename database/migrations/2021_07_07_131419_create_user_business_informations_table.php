<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserBusinessInformationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_business_informations', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->string('businessName');
            $table->string('businessCode');
            $table->string('businessCategory');
            $table->string('registrationStatus');
            $table->string('startYear');
            $table->string('businessProvince');
            $table->string('businessDistrict');
            $table->string('businessSector');
            $table->string('businessCell');
            $table->string('businessVillage');
            $table->string('businessType');
            $table->string('SubbusinessType_id');
            $table->string('ChildSubbusinessType_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_business_informations');
    }
}
